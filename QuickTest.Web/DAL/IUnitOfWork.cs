﻿using System;
using QuickTest.Web.DAL.Repositories;

namespace QuickTest.Web.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        StudentRepository StudentRepository { get; }
        StudentTestAttempRepository StudentTestAttempRepository { get; }
        TaskSolutionRepository TaskSolutionRepository { get; }
        TeacherRepository TeacherRepository { get; }
        TestRepository TestRepository { get; }
        TestingTaskRepository TestingTaskRepository { get; }
        TestSessionRepository TestSessionRepository { get; }
        JudgeTokenRepository JudgeTokenRepository { get; }
        void Save();
    }
}
