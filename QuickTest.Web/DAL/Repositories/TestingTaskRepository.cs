﻿using System.Linq;
using Microsoft.Data.Entity;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.DAL.Repositories
{
    public class TestingTaskRepository : BaseRepository<TestingTask>
    {
        public TestingTaskRepository(QuickTestDbContext context) : base(context)
        {
        }

        public override TestingTask GetByID(int id)
        {
            return
                DbSet
                    .Include(item => item.TaskSolutions)
                    .Include(item => item.Test)
                    .SingleOrDefault(item => item.ID == id);
        }
    }
}
