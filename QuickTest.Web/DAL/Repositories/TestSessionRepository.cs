﻿using System.Linq;
using Microsoft.Data.Entity;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.DAL.Repositories
{
    public class TestSessionRepository : BaseRepository<TestSession>
    {
        public TestSessionRepository(QuickTestDbContext context) : base(context)
        {
        }

        protected override IQueryable<TestSession> IncludedAllDbSet
        {
            get
            {
                return DbSet.Include(s=>s.StudentAttemps);
            }
        }

        public override TestSession GetByID(int id)
        {
            return
                DbSet
                    .Include(item => item.Creator)
                    .Include(item => item.StudentAttemps)
                    .Include(item => item.Test)
                    .Include(item=>item.Test.TestingTasks)
                    .SingleOrDefault(item => item.ID == id);
        }
    }
}
