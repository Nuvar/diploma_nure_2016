﻿using System.Linq;
using Microsoft.Data.Entity;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.DAL.Repositories
{
    public class StudentRepository : BaseRepository<Student>
    {
        public StudentRepository(QuickTestDbContext context) : base(context)
        {
        }

        public override Student GetByID(int id)
        {
            return
                DbSet
                    .Include(item => item.TestAttemps)
                    .SingleOrDefault(item => item.ID == id);
        }
    }
}
