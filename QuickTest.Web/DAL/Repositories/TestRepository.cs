﻿using System.Linq;
using Microsoft.Data.Entity;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.DAL.Repositories
{
    public class TestRepository : BaseRepository<Test>
    {
        public TestRepository(QuickTestDbContext context) : base(context)
        {
        }

        protected override IQueryable<Test> IncludedAllDbSet
        {
            get
            {
                return DbSet
                    .Include(item => item.Author)
                    .Include(item => item.TestSessions)
                    .Include(item => item.TestingTasks);
            }
        }

        public override Test GetByID(int id)
        {
            return
                DbSet
                    .Include(item => item.Author)
                    .Include(item => item.TestSessions)
                    .Include(item => item.TestingTasks)
                    .SingleOrDefault(item => item.ID == id);
        }
    }
}
