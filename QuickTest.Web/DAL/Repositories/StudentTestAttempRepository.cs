﻿using System.Linq;
using Microsoft.Data.Entity;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.DAL.Repositories
{
    public class StudentTestAttempRepository : BaseRepository<StudentTestAttemp>
    {
        public StudentTestAttempRepository(QuickTestDbContext context) : base(context)
        {
        }

        public override StudentTestAttemp GetByID(int id)
        {
            return
                DbSet
                    .Include(item => item.Student)
                    .Include(item => item.TaskSolutions)
                    .Include(item => item.TeacherWhoAllowed)
                    .Include(item => item.TestSession)
                    .SingleOrDefault(item => item.ID == id);
        }
    }
}
