﻿using System.Linq;
using Microsoft.Data.Entity;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.DAL.Repositories
{
    public class TeacherRepository : BaseRepository<Teacher>
    {
        public TeacherRepository(QuickTestDbContext context) : base(context)
        {
        }

        public override Teacher GetByID(int id)
        {
            return
                DbSet
                    .Include(item => item.JudgeToken)
                    .Include(item => item.AllowedStudentsTestAttemps)
                    .Include(item => item.CreatedTests)
                    .Include(item => item.TestSessions)
                    .SingleOrDefault(item => item.ID == id);
        }
    }
}
