﻿using System.Linq;
using Microsoft.Data.Entity;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.DAL.Repositories
{
    public class TaskSolutionRepository : BaseRepository<TaskSolution>
    {
        public TaskSolutionRepository(QuickTestDbContext context) : base(context)
        {
        }

        public override TaskSolution GetByID(int id)
        {
            return
                DbSet
                    .Include(item => item.StudentTestAttemp)
                    .Include(item => item.TestingTask)
                    .SingleOrDefault(item => item.ID == id);
        }
    }
}
