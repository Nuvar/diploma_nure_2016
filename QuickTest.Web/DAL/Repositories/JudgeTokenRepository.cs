﻿using System.Linq;
using Microsoft.Data.Entity;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.DAL.Repositories
{
    public class JudgeTokenRepository : BaseRepository<JudgeToken>
    {
        public JudgeTokenRepository(QuickTestDbContext context) : base(context)
        {
        }

        protected override IQueryable<JudgeToken> IncludedAllDbSet
        {
            get
            {
                return DbSet.Include(item => item.Teacher);
            }
        }

        public override JudgeToken GetByID(int id)
        {
            return
                DbSet
                    .Include(item => item.Teacher)
                    .SingleOrDefault(item => item.ID == id);
        }
    }
}
