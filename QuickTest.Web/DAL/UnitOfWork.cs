﻿using System;
using System.Linq;
using QuickTest.Web.DAL.Repositories;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly QuickTestDbContext _context;

        private StudentRepository _studentRepository;
        private StudentTestAttempRepository _studentTestAttempRepository;
        private TaskSolutionRepository _taskSolutionRepository;
        private TeacherRepository _teacherRepository;
        private TestRepository _testRepository;
        private TestingTaskRepository _testingTaskRepository;
        private TestSessionRepository _testSessionRepository;
        private JudgeTokenRepository _judgeTokenRepository;

        public StudentRepository StudentRepository
            => _studentRepository ?? (_studentRepository = new StudentRepository(_context));

        public StudentTestAttempRepository StudentTestAttempRepository
            => _studentTestAttempRepository ?? (_studentTestAttempRepository = new StudentTestAttempRepository(_context));

        public TaskSolutionRepository TaskSolutionRepository
            => _taskSolutionRepository ?? (_taskSolutionRepository = new TaskSolutionRepository(_context));

        public TeacherRepository TeacherRepository
            => _teacherRepository ?? (_teacherRepository = new TeacherRepository(_context));

        public TestRepository TestRepository
            => _testRepository ?? (_testRepository = new TestRepository(_context));

        public TestingTaskRepository TestingTaskRepository
            => _testingTaskRepository ?? (_testingTaskRepository = new TestingTaskRepository(_context));

        public TestSessionRepository TestSessionRepository
            => _testSessionRepository ?? (_testSessionRepository = new TestSessionRepository(_context));

        public JudgeTokenRepository JudgeTokenRepository
            => _judgeTokenRepository ?? (_judgeTokenRepository = new JudgeTokenRepository(_context));

        public UnitOfWork(QuickTestDbContext context)
        {
            _context = context;
        }

        public void InsertTeacherWithJudgeToken(Teacher teacher, JudgeToken judgeToken)
        {
            // Create Teacher in DB
            TeacherRepository.Insert(teacher);
            Save();
            var teacherName = teacher.UserName;
            var createdTeacher =
                TeacherRepository.Get(t => string.Equals(t.UserName, teacherName, StringComparison.OrdinalIgnoreCase))
                    .Single();

            // Create Token in DB
            judgeToken.Teacher = createdTeacher;
            JudgeTokenRepository.Insert(judgeToken);
            Save();
            var createdJudgeToken = JudgeTokenRepository.Get(t => t.Teacher.ID == createdTeacher.ID).Single();

            // Update teacher with token
            createdTeacher.JudgeToken = createdJudgeToken;
            TeacherRepository.Update(createdTeacher);
            Save();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
