﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.DAL
{
    public interface IRepository<TEntity>
        where TEntity : BaseEntity
    {
        IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null
            //,string includeProperties = ""
            );

        TEntity GetByID(int id);

        void Insert(TEntity item);

        bool Delete(int id);

        bool Delete(TEntity item);

        void Update(TEntity itemToUpdate);

    }
}
