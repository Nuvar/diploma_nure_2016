﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.Data.Entity;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.DAL
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        internal QuickTestDbContext Context;
        internal DbSet<TEntity> DbSet;
        protected virtual IQueryable<TEntity> IncludedAllDbSet
        {
            get
            {
                return DbSet;
            }
        }

        protected BaseRepository(QuickTestDbContext context)
        {
            Context = context;
            DbSet = context.Set<TEntity>();
        }

        public virtual IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null
            )
        {
            IQueryable<TEntity> query = IncludedAllDbSet;
            

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }

            return query.ToList();
        }

        public virtual TEntity GetByID(int id)
        {
            return IncludedAllDbSet.SingleOrDefault(item => item.ID == id);
        }

        public virtual void Insert(TEntity item)
        {
            item.CreatedDate = DateTime.UtcNow;
            item.ModifiedDate = DateTime.UtcNow;
            DbSet.Add(item);
        }

        public virtual bool Delete(int id)
        {
            var entityToDelete = GetByID(id);
            Delete(entityToDelete);
            return true;
        }

        public virtual bool Delete(TEntity item)
        {
            if (Context.Entry(item).State == EntityState.Detached)
            {
                DbSet.Attach(item);
            }
            DbSet.Remove(item);
            return true;
        }

        public virtual void Update(TEntity itemToUpdate)
        {
            var createdDate = GetByID(itemToUpdate.ID).CreatedDate;
            itemToUpdate.CreatedDate = createdDate;
            itemToUpdate.ModifiedDate = DateTime.UtcNow;
            DbSet.Attach(itemToUpdate);
            Context.Entry(itemToUpdate).State = EntityState.Modified;
        }
    }
}
