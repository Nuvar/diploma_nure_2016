﻿using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.DAL
{
    public sealed class QuickTestDbContext : DbContext
    {
        public QuickTestDbContext()
        {
            Database.EnsureCreated();
        }

        public DbSet<TestingTask> TestingTask { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<TestSession> TestSessions { get; set; }
        public DbSet<StudentTestAttemp> StudentTestAttemps { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<TaskSolution> TaskSolutions { get; set; }
        public DbSet<JudgeToken> JudgeTokens { get; set; }

        public QuickTestDbContext(DbContextOptions<QuickTestDbContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Test>()
                .HasOne(entity => entity.Author)
                .WithMany(entity => entity.CreatedTests)
                .HasForeignKey(entity => entity.AuthorID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TestingTask>()
                .HasOne(entity => entity.Test)
                .WithMany(entity => entity.TestingTasks)
                .HasForeignKey(entity => entity.TestID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TestSession>()
                .HasOne(entity => entity.Test)
                .WithMany(entity => entity.TestSessions)
                .HasForeignKey(entity => entity.TestID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TestSession>()
                .HasOne(entity => entity.Creator)
                .WithMany(entity => entity.TestSessions)
                .HasForeignKey(entity => entity.CreatorID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<StudentTestAttemp>()
                .HasOne(entity => entity.TeacherWhoAllowed)
                .WithMany(entity => entity.AllowedStudentsTestAttemps)
                .HasForeignKey(entity => entity.TeacherWhoAllowedID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<StudentTestAttemp>()
                .HasOne(entity => entity.Student)
                .WithMany(entity => entity.TestAttemps)
                .HasForeignKey(entity => entity.StudentID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<StudentTestAttemp>()
                .HasOne(entity => entity.TestSession)
                .WithMany(entity => entity.StudentAttemps)
                .HasForeignKey(entity => entity.TestSessionID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TaskSolution>()
                .HasOne(entity => entity.StudentTestAttemp)
                .WithMany(entity => entity.TaskSolutions)
                .HasForeignKey(entity => entity.StudentTestAttempID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TaskSolution>()
                .HasOne(entity => entity.TestingTask)
                .WithMany(entity => entity.TaskSolutions)
                .HasForeignKey(entity => entity.TestingTaskID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Teacher>()
                .HasOne(entity => entity.JudgeToken)
                .WithOne()
                .HasForeignKey<Teacher>(entity => entity.JudgeTokenID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<JudgeToken>()
                .HasOne(entity => entity.Teacher)
                .WithOne()
                .OnDelete(DeleteBehavior.Restrict);

            base.OnModelCreating(modelBuilder);
        }
    }
}
