﻿namespace QuickTest.Web.Models
{
    public class RequestResult<T>
    {
        public bool Success { get; set; }
        public int StatusCode { get; set; }
        public ErrorModel Error { get; set; }
        public T Result { get; set; }

        public RequestResult(int statusCode, ErrorModel error)
        {
            Success = false;
            StatusCode = statusCode;
            Error = error;
            Result = default(T);
        }

        public RequestResult(int statusCode, T result)
        {
            Success = true;
            StatusCode = statusCode;
            Error = null;
            Result = result;
        }
    }
}
