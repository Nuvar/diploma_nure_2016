﻿using System;

namespace QuickTest.Web.Models
{
    public enum UserRole
    {
        Teacher,
        Student
    }

    public class TokenModel
    {
        public bool Authenticated { get; }
        public string EntityName { get; }
        public int EntityID { get; }
        public UserRole? UserRole { get; }
        public string Token { get; }
        public DateTime Expires { get; }

        /// <summary>
        /// Not authenticated constructor
        /// </summary>
        public TokenModel()
        {
            UserRole = null;
            Authenticated = false;
            EntityName = "";
            EntityID = 0;
            Token = "";
            Expires = DateTime.MinValue;
        }
        
        /// <summary>
        /// Authenticated constructor
        /// </summary>
        public TokenModel(string entityName, int entityId, UserRole userRole, string token, DateTime expires)
        {
            Authenticated = true;
            EntityName = entityName;
            EntityID = entityId;
            UserRole = userRole;
            Token = token;
            Expires = expires;
        }
    }
}
