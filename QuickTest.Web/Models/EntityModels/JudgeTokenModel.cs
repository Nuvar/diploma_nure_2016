﻿using System;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.Models.EntityModels
{
    public class JudgeTokenModel
    {
        public DateTime Expires { get; set; }
        public DateTime? Issued { get; set; }
        public string AccessToken { get; set; }
        public int? ExpiresIn { get; set; }
        public string TokenType { get; set; }
        public string UserName { get; set; }

        public JudgeToken ToEntity()
        {
            return new JudgeToken
            {
                AccessToken = this.AccessToken,
                Expires = this.Expires,
                ExpiresIn = this.ExpiresIn,
                Issued = this.Issued,
                TokenType = this.TokenType,
                UserName = this.UserName
            };
        }
    }
}
