﻿using System;
using System.Text.RegularExpressions;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.Models.EntityModels
{
    public class StudentModel
    {
        private const string NameRegexp =
            @"^([A-Z]|[А-Я]|І|Ґ|Ї|Є)([A-Z]|[А-Я]|І|Ґ|Ї|Є|[a-z]|[а-я]|і|ґ|ї|є|')+" + // Second Name
            @" ([A-Z]|[А-Я]|І|Ґ|Ї|Є)([A-Z]|[А-Я]|І|Ґ|Ї|Є|[a-z]|[а-я]|і|ґ|ї|є|')+$"; // First Name

        public string Name { get; set; }
        public string Group { get; set; }

        public StudentModel()
        {
        }

        public StudentModel(Student student)
        {
            Name = student.Name;
            Group = student.Group;
        }

        public Student ToEntity()
        {
            return new Student
            {
                Name = this.Name,
                Group = this.Group
            };
        }

        public bool IsRightName()
        {
            return Regex.IsMatch(Name, NameRegexp);
        }

        public bool IsSameNameWith(string name)
        {
            var firstAndSecondName = Name.Split(' ');
            var swappedFirstWithSecondName = firstAndSecondName[1] + " " + firstAndSecondName[0];
            return string.Equals(Name, name, StringComparison.OrdinalIgnoreCase) ||
                   string.Equals(Name, swappedFirstWithSecondName, StringComparison.OrdinalIgnoreCase);
        }
    }
}