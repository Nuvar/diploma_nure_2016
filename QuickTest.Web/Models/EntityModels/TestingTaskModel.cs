﻿using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.Models.EntityModels
{
    public class TestingTaskModel
    {
        public int TaskID { get; set; }
        public int TestID { get; set; }

        public TestingTaskModel()
        {

        }

        public TestingTask ToEntity()
        {
            return new TestingTask
            {
                TaskID = this.TaskID,
                TestID = this.TestID
            };
        }
    }
}
