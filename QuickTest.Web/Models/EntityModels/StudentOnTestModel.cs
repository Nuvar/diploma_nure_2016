﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickTest.Web.Models.EntityModels
{
    public class StudentOnTestModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
