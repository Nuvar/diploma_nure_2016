﻿using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.Models.EntityModels
{
    public class TeacherModel
    {
        public string UserName { get; set; }
        public bool? HasRegistered { get; set; }
        public string LoginProvider { get; set; }
        public int? JudgeTokenID { get; set; }

        public TeacherModel()
        {
        }

        public TeacherModel(Teacher teacher)
        {
            UserName = teacher.UserName;
            HasRegistered = teacher.HasRegistered;
            LoginProvider = teacher.LoginProvider;
            JudgeTokenID = teacher.JudgeTokenID;
        }

        public Teacher ToEntity()
        {
            return new Teacher
            {
                UserName = this.UserName,
                HasRegistered = this.HasRegistered,
                LoginProvider = this.LoginProvider,
                JudgeTokenID = this.JudgeTokenID,
            };
        }
    }
}
