﻿using System;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.Models.EntityModels
{
    public class StudentTestAttempModel
    {
        public DateTime? StartTime { get; set; }
        public bool IsUsed { get; set; }
        public int StudentID { get; set; }
        public int TestSessionID { get; set; }

        public StudentTestAttempModel()
        {

        }

        public StudentTestAttemp ToEntity()
        {
            return new StudentTestAttemp
            {
                StartTime = this.StartTime,
                IsUsed = this.IsUsed,
                StudentID = this.StudentID,
                TestSessionID = this.TestSessionID
            };
        }
    }
}
