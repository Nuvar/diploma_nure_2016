﻿namespace QuickTest.Web.Models.EntityModels
{
    public class TaskInfoModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Attr { get; set; }
        public string Lang { get; set; }
        public string Cond { get; set; }
        public string View { get; set; }
    }
}
