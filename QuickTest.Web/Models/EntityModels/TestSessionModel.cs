﻿using System;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.Models.EntityModels
{
    public class TestSessionModel
    {
        public string Name { get; set; }
        public bool IsOpened { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int TestTime { get; set; }
        public int TestID { get; set; }

        public string Students { get; set; }

        public TestSessionModel()
        {

        }

        public TestSession ToEntity()
        {
            
            return new TestSession
            {
                StartTime = this.StartTime,
                EndTime = IsOpened? null :new DateTime?(DateTime.Now),
                TestTime = TimeSpan.FromMinutes(TestTime),
                Name = this.Name,
                TestID = this.TestID,
                
            };

            
        }
    }
}
