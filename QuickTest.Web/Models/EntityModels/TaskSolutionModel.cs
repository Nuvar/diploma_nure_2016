﻿using System;
using QuickTest.Web.Models.Entities;

namespace QuickTest.Web.Models.EntityModels
{
    public class TaskSolutionModel
    {
        public bool IsSubmitted { get; set; }
        public string Answer { get; set; }
        public string CheckResult { get; set; }
        public int StudentTestAttempID { get; set; }
        public int TestingTaskID { get; set; }

        public TaskSolutionModel()
        {
        }

        public TaskSolutionModel(TaskSolution taskSolution)
        {
            IsSubmitted = taskSolution.IsSubmitted;
            Answer = taskSolution.Answer;
            CheckResult = taskSolution.CheckResult;
            StudentTestAttempID = taskSolution.StudentTestAttempID;
            TestingTaskID = TestingTaskID;
        }

        public TaskSolution ToEntity()
        {
            return new TaskSolution
            {
                IsSubmitted = this.IsSubmitted,
                Answer = this.Answer,
                CheckResult = this.CheckResult,
                StudentTestAttempID = this.StudentTestAttempID,
                TestingTaskID = this.TestingTaskID,
            };
        }
    }
}
