﻿namespace QuickTest.Web.Models.EntityModels
{
    public class TaskFullModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Attr { get; set; }
        public string Lang { get; set; }
        public string Cond { get; set; }
        public string View { get; set; }
        public string Hint { get; set; }
        public string Code { get; set; }
    }
}
