﻿using System;
using QuickTest.Web.Models.Entities;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace QuickTest.Web.Models.EntityModels
{
    public class TestModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int EstimatedTimeToPass { get; set; }
        public List<TestingTaskModel> TestingTasks { get; set; }

        public TestModel()
        {
            TestingTasks = new List<TestingTaskModel>();
        }

        public Test ToEntity()
        {
            return new Test
            {
                Title = this.Title,
                EstimatedTimeToPass = TimeSpan.FromMinutes(EstimatedTimeToPass),
                TestingTasks = TestingTasks.Select(t => t.ToEntity()).ToList()
            };
        }
    }
}
