﻿namespace QuickTest.Web.Models
{
    public class TeacherCredentials
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
