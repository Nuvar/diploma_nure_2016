﻿using System;
using System.ComponentModel.DataAnnotations;

namespace QuickTest.Web.Models.Entities
{
    public abstract class BaseEntity
    {
        [Key]
        public int ID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
