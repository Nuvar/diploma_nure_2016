﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickTest.Web.Models.Entities
{
    public class StudentTestAttemp : BaseEntity
    {
        public DateTime? StartTime { get; set; }
        public bool IsUsed { get; set; }
        public int TeacherWhoAllowedID { get; set; }
        public int StudentID { get; set; }
        public int TestSessionID { get; set; }

        [ForeignKey("TeacherWhoAllowedID")]
        public virtual Teacher TeacherWhoAllowed { get; set; }
        [ForeignKey("StudentID")]
        public virtual Student Student { get; set; }
        [ForeignKey("TestSessionID")]
        public virtual TestSession TestSession { get; set; }
        
        public virtual ICollection<TaskSolution> TaskSolutions { get; set; }
    }
}
