﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickTest.Web.Models.Entities
{
    public class TestingTask : BaseEntity
    {
        public int TaskID { get; set; }
        public int TestID { get; set; }

        [ForeignKey("TestID")]
        public virtual Test Test { get; set; }

        public virtual ICollection<TaskSolution> TaskSolutions { get; set; }
    }
}
