﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickTest.Web.Models.Entities
{
    public class Teacher : BaseEntity
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        public bool? HasRegistered { get; set; }
        public string LoginProvider { get; set; }
        public int? JudgeTokenID { get; set; }

        [ForeignKey("JudgeTokenID")]
        public virtual JudgeToken JudgeToken { get; set; }

        public virtual ICollection<StudentTestAttemp> AllowedStudentsTestAttemps { get; set; }
        public virtual ICollection<Test> CreatedTests { get; set; }
        public virtual ICollection<TestSession> TestSessions { get; set; } 
    }
}
