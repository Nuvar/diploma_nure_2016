﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace QuickTest.Web.Models.Entities
{
    public class SocketCommandEventArgs : EventArgs
    {
        public string CommandName { get; set; }

        public string[] Args { get; set; }
    }

    public delegate void CommandReceivedEventHandler(HubClient sourceClient, SocketCommandEventArgs eventArgs);

    public class HubClient
    {
        public int ReferenceEntityId { get; set; }
        public UserRole Role { get; set; }
        public WebSocket Socket { get; set; }

        public event CommandReceivedEventHandler CommandReceived;

        public HubClient(int referenceEntityId, UserRole role, WebSocket socket)
        {
            ReferenceEntityId = referenceEntityId;
            Role = role;
            Socket = socket;
        }

        public async Task StartBroadcasting()
        {
            while (Socket.State == WebSocketState.Open)
            {
                var token = CancellationToken.None;
                var buffer = new ArraySegment<byte>(new byte[4096]);

                // Below will wait for a request message.
                var received = await Socket.ReceiveAsync(buffer, token);
                switch (received.MessageType)
                {
                    case WebSocketMessageType.Text:
                        string request =
                            Encoding.UTF8.GetString(
                                buffer.Array,
                                buffer.Offset,
                                buffer.Count);
                        var json = new JsonSerializer();
                        var command = (SocketCommandEventArgs)json.Deserialize(new StringReader(request), typeof(SocketCommandEventArgs));
                        if(CommandReceived != null)
                        {
                            CommandReceived(this, command);
                        }
                        break;
                }
            }

        }
    }
}
