﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickTest.Web.Models.Entities
{
    public class TestSession : BaseEntity
    {
        public string Name { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public TimeSpan TestTime { get; set; }
        public int TestID { get; set; }
        public int CreatorID { get; set; }

        [NotMapped]
        public bool IsOpened
        {
            get
            {
                return EndTime == null;
            }
        }

        [ForeignKey("TestID")]
        public virtual Test Test { get; set; }
        [ForeignKey("CreatorID")]
        public virtual Teacher Creator { get; set; }
        
        public virtual ICollection<StudentTestAttemp> StudentAttemps { get; set; }
    }
}