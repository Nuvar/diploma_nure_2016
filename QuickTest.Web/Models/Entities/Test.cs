﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickTest.Web.Models.Entities
{
    public class Test : BaseEntity
    {
        [Required]
        public string Title { get; set; }
        public TimeSpan? EstimatedTimeToPass { get; set; }
        public int AuthorID { get; set; }

        [ForeignKey("AuthorID")]
        public virtual Teacher Author { get; set; }
        
        public virtual ICollection<TestSession> TestSessions { get; set; }
        public virtual ICollection<TestingTask> TestingTasks { get; set; }
    }
}
