﻿using System;
using System.ComponentModel.DataAnnotations;

namespace QuickTest.Web.Models.Entities
{
    public class JudgeToken : BaseEntity
    {
        public DateTime Expires { get; set; }
        public DateTime? Issued { get; set; }
        [Required]
        public string AccessToken { get; set; }
        public int? ExpiresIn { get; set; }
        public string TokenType { get; set; }
        public string UserName { get; set; }

        public virtual Teacher Teacher { get; set; }
    }
}
