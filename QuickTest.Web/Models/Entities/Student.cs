﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QuickTest.Web.Models.Entities
{
    public class Student : BaseEntity
    {
        [Required]
        public string Name { get; set; }
        public string Group { get; set; }
        
        public virtual ICollection<StudentTestAttemp> TestAttemps { get; set; }
    }
}
