﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickTest.Web.Models.Entities
{
    public class TaskSolution : BaseEntity
    {
        public bool IsSubmitted { get; set; }
        [Required]
        public string Answer { get; set; }
        public string CheckResult { get; set; }
        public int StudentTestAttempID { get; set; }
        public int TestingTaskID { get; set; }

        [ForeignKey("StudentTestAttempID")]
        public virtual StudentTestAttemp StudentTestAttemp { get; set; }
        [ForeignKey("TestingTaskID")]
        public virtual TestingTask TestingTask { get; set; }

    }
}
