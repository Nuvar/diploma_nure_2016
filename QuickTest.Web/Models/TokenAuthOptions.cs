﻿using System.IdentityModel.Tokens;

namespace QuickTest.Web.Models
{
    public class TokenAuthOptions
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public SigningCredentials SigningCredentials { get; set; }
    }
}
