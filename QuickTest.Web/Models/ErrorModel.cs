﻿namespace QuickTest.Web.Models
{
    public class ErrorModel
    {
        public string Error { get; set; }
        public string ErrorDescription { get; set; }

        public ErrorModel()
        { }

        public ErrorModel(string error, string errorDescription)
        {
            Error = error;
            ErrorDescription = errorDescription;
        }
    }
}
