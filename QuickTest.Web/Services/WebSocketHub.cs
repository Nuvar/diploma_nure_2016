﻿using Microsoft.AspNet.Http;
using Microsoft.CodeAnalysis;
using Newtonsoft.Json;
using QuickTest.Web.DAL;
using QuickTest.Web.Logic;
using QuickTest.Web.Models;
using QuickTest.Web.Models.Entities;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QuickTest.Web.Services
{

    public static class WebSocketHub
    {
        private static readonly ConcurrentDictionary<string, HubClient> _sockets = new ConcurrentDictionary<string, HubClient>();
        private static readonly ConcurrentBag<Student> _studentsInQueue = new ConcurrentBag<Student>();
        public static async Task InitializaeHub(HttpContext context, Func<Task> next)
        {
            var http = context;

            if (http.WebSockets.IsWebSocketRequest)
            {
                string accessToken = http.Request.Query["access_token"];
                Claim[] claims = TokenManager.ReadUserFromToken(accessToken).ToArray();
                var webSocket = await http.WebSockets.AcceptWebSocketAsync();

                if (webSocket != null && webSocket.State == WebSocketState.Open)
                {
                    var connectionId = Guid.NewGuid().ToString("N");
                    var hubClient = new HubClient(Convert.ToInt32(claims[0].Value), (UserRole)Enum.Parse(typeof(UserRole), claims[1].Value), webSocket);
                    if (_sockets.TryAdd(connectionId, hubClient))
                    {
                        hubClient.CommandReceived += HandleCommand;
                        await hubClient.StartBroadcasting();
                        hubClient.CommandReceived -= HandleCommand;
                        HubClient closedSocket;
                        _sockets.TryRemove(connectionId, out closedSocket);
                    }
                }
                else
                {
                    await next();
                }
            }
        }

        public static void HandleCommand(HubClient client, SocketCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "EnqueueStudent": NewStudentAccepted(client); break;
                case "AssignSession": AssignSessionToStudent(client, e.Args); break;
                case "CloseSession": CloseSession(client, e.Args); break;
            }
        }

        private static async void NewStudentAccepted(HubClient client)
        {
            if (client.Role != UserRole.Student) throw new InvalidOperationException();

            using (var context = new QuickTestDbContext())
            {
                using (IUnitOfWork dataStorage = new UnitOfWork(context))
                {
                    Student student = dataStorage.StudentRepository.GetByID(client.ReferenceEntityId);

                    _studentsInQueue.Add(student);
                    string command = CommandToJson("StudentAccepted", student.ID.ToString(), student.Name, student.Group);

                    foreach (HubClient teacher in _sockets.Where(s => s.Value.Role == UserRole.Teacher).Select(s => s.Value))
                    {
                        await teacher.Socket.SendTextAsync(command);
                    }
                }
            }
        }

        private static async void AssignSessionToStudent(HubClient client, string[] args)
        {
            if (client.Role != UserRole.Teacher) throw new InvalidOperationException();

            int studentId = Convert.ToInt32(args[0]);
            int sessionId = Convert.ToInt32(args[1]);

            var studentAttemp = new StudentTestAttemp();
            studentAttemp.StudentID = studentId;
            studentAttemp.TeacherWhoAllowedID = client.ReferenceEntityId;
            studentAttemp.TestSessionID = sessionId;
            var student = _sockets.FirstOrDefault(s => s.Value.ReferenceEntityId == studentId);
            if (student.Value == null) return;

            using (var context = new QuickTestDbContext())
            {
                using (IUnitOfWork dataStorage = new UnitOfWork(context))
                {
                    dataStorage.StudentTestAttempRepository.Insert(studentAttemp);
                }
            }

            await student.Value.Socket.SendTextAsync(CommandToJson("AssignSession", sessionId.ToString()));
        }

        private static async void CloseSession(HubClient client, string[] args)
        {

            int sessionId = Convert.ToInt32(args[0]);
            int studentId = args.Length > 1 ? Convert.ToInt32(args[1]) : 0;

            using (var context = new QuickTestDbContext())
            {
                using (IUnitOfWork dataStorage = new UnitOfWork(context))
                {
                    if (studentId > 0)
                    {
                        var studentClient = _sockets.FirstOrDefault(s => s.Value.ReferenceEntityId == studentId).Value;

                        await CloseSessionForStudent(studentClient, dataStorage, sessionId, studentId);
                    }
                    else
                    {
                        var studentsOnTest = dataStorage.StudentTestAttempRepository.Get(s => !s.IsUsed && s.TestSessionID == sessionId);
                        foreach (StudentTestAttemp testAttemp in studentsOnTest)
                        {
                            testAttemp.IsUsed = true;
                            dataStorage.StudentTestAttempRepository.Update(testAttemp);
                            HubClient tartgetClient = _sockets.FirstOrDefault(s => s.Value.ReferenceEntityId == testAttemp.StudentID).Value;
                            if (tartgetClient != null)
                            {
                                await tartgetClient.Socket.SendTextAsync(CommandToJson("CloseSession"));
                            }
                        }
                        var sessionToClose = dataStorage.TestSessionRepository.GetByID(sessionId);
                        sessionToClose.EndTime = DateTime.Now;
                        dataStorage.TestSessionRepository.Update(sessionToClose);
                        dataStorage.Save();
                    }
                }
            }
        }

        private static async Task CloseSessionForStudent(HubClient studentClient, IUnitOfWork unitOfWork, int sessionId, int studentId)
        {
            StudentTestAttemp testAttemp = unitOfWork.StudentTestAttempRepository.Get(studentAttemp => studentAttemp.StudentID == studentId && studentAttemp.TestSessionID == sessionId).FirstOrDefault();
            if (testAttemp != null)
            {
                testAttemp.IsUsed = true;
                unitOfWork.StudentTestAttempRepository.Update(testAttemp);
                unitOfWork.Save();
            }

            if (studentClient != null)
            {
                await studentClient.Socket.SendTextAsync(CommandToJson("CloseSession"));
            }
        }

        private static string CommandToJson(string commandName, params string[] args)
        {
            var command = new SocketCommandEventArgs
            {
                CommandName = commandName,
                Args = args
            };

            var serializer = new JsonSerializer();
            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, command, typeof(SocketCommandEventArgs));
                return writer.ToString();
            }
        }
    }
}
