﻿

export class Helper {
    static getEventTarget(event) {
        if (event.target) {
            return event.target;
        }
        return event.srcElement;
    }

    static getIndexOf(array: Array<any>, checkExpression: (item: any) => boolean) {
        for (var i = 0; i < array.length; i++) {
            if (checkExpression(array[i])) {
                return i;
            }
        }
        return -1;
    }
}