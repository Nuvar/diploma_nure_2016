﻿import {DataHandlersModel, DataServiceHandlerIdentifier, DataHandlerModel, HttpMethods} from './../../models/data.handlers.model';
import {CacheModel} from './../../models/cache.model';
import {Http, HTTP_PROVIDERS, Headers} from 'angular2/http';
import {Inject} from 'angular2/core';
import {Cookie} from './../../helpers/cookies';
import {Observer} from 'rxjs/Observer';
import 'rxjs/add/operator/map';
import {TaskModel} from './../../models/task.model';
declare var jQuery;
export class DataService {
    private handlersModel: DataHandlersModel;
    private http: Http;
    private static cache: Array<CacheModel> = [];
    constructor( @Inject(Http) http) {
        this.handlersModel = new DataHandlersModel();
        this.http = http;
    }

    private static clearCache(id: DataServiceHandlerIdentifier) {
        var cache: CacheModel = jQuery.grep(DataService.cache, function (item: CacheModel) {
            item.id == DataServiceHandlerIdentifier.GetTests;
        });
        var index: number = DataService.cache.indexOf(cache);
        DataService.cache.splice(index, 1);
    }

    getFromCacheTask(id: number): TaskModel {
        var cache: CacheModel = jQuery.grep(DataService.cache, function (item: CacheModel) {
            item.id == DataServiceHandlerIdentifier.GetTasks;
        });
        var task: TaskModel = null;
        if (cache) {
            task = jQuery.grep(cache.chachedObject, function (item) { item.Id == id; });
        }
        return task;
    }

    public getTask(id: number, handler: (value: TaskModel) => void) {

        this.performRequest(DataServiceHandlerIdentifier.GetTask, value => {
            handler(value);
        }, false, id);

    }

    public performRequest(identifier: DataServiceHandlerIdentifier, observableOrNext: ((value: any) => void), useCache: boolean, id?: number, data?: string, withoutJson?: boolean): void {
        var handler: DataHandlerModel = this.handlersModel.getHandlerByIdentifier(identifier);
        var option = new Headers();
        var auth = Cookie.load('Auth');
        //if (!auth || auth.length == 0) {
        //    location.href = '/SignIn';
        //    return;
        //}
        option.append('Authorization', auth);
        option.append('Content-Type', 'application/json;charset=utf8');
        if (handler !== null) {
            var cache = DataService.cache.find((value, index) => { return value.id == identifier });
            if (cache != null && useCache) {
                observableOrNext(cache.chachedObject);
            }

            var idString = id ? id.toString() : '';
            switch (handler.method) {
                case HttpMethods.GET:
                    this.http.get(handler.url + idString, { headers: option })
                        .map(res => res.json())
                        .subscribe(value => {
                            if (idString === '' && useCache) {
                                DataService.cache.push(new CacheModel(identifier, value));
                            }
                            console.log(value);
                            observableOrNext(value);
                        }, value => {

                        });
                    break;
                case HttpMethods.POST:
                    this.http.post(handler.url + idString, data, { headers: option })
                        .map(res => res.status == 200 ? (withoutJson ? res.text() : res.json()) : res.statusText)
                        .subscribe(observableOrNext);
                    if (identifier == DataServiceHandlerIdentifier.SaveTest) DataService.clearCache(DataServiceHandlerIdentifier.GetTests);
                    break;
                case HttpMethods.PUT:
                    this.http.put(handler.url + idString, data, { headers: option })
                        .map(res => res.status == 200 ? '' : res.statusText)
                        .subscribe(observableOrNext);
                    if (identifier == DataServiceHandlerIdentifier.UpdateTest) DataService.clearCache(DataServiceHandlerIdentifier.GetTests);
                    break;
                case HttpMethods.DELETE:
                    this.http.delete(handler.url + idString, { headers: option })
                        .map(res => res.ok ? '' : res.statusText)
                        .subscribe(observableOrNext);
                    break;
            }
        }
    }
}
