"use strict";
var angular2_websocket_1 = require('angular2-websocket/angular2-websocket');
var cookies_1 = require('./../../helpers/cookies');
var WebSocketClient = (function () {
    function WebSocketClient() {
        this.handlers = {};
    }
    Object.defineProperty(WebSocketClient.prototype, "isConnected", {
        get: function () {
            return this.socket.getReadyState() === 1;
        },
        enumerable: true,
        configurable: true
    });
    WebSocketClient.prototype.attachMessageListenear = function (commandName, callback) {
        this.handlers[commandName] = callback;
    };
    WebSocketClient.prototype.detachMessageListenear = function (commandName) {
        this.handlers[commandName] = null;
    };
    WebSocketClient.prototype.connect = function (closeHandler) {
        var _this = this;
        this.socket = new angular2_websocket_1.$WebSocket('ws://' + location.host + '/Communication?access_token=' + cookies_1.Cookie.load('Auth'), undefined, { initialTimeout: 10000, maxTimeout: 30000, reconnectIfNotNormalClose: true });
        this.socket.connect();
        this.socket.onClose(function (ev) {
            if (ev.code != 1000) {
                _this.checkReconnect(1);
            }
            else if (closeHandler) {
                closeHandler();
            }
        });
        this.socket.onMessage(function (data) {
            var callback = _this.handlers[data.command];
            if (callback) {
                callback.apply(_this, [data.args]);
            }
        }, undefined);
    };
    WebSocketClient.prototype.checkReconnect = function (attemp) {
        var _this = this;
        if (this.isConnected) {
            noty({ text: 'Связь восстановлена!', type: 'success', killer: true, timeout: 5000, layout: 'topRight' });
        }
        else if (attemp == 5) {
            noty({ text: 'Не удается соединится с сервером.<br/>Попробуйте перезагрузит страницу. Если ошибка остается, свяжитесь с вашим системным администратором.', type: 'error', killer: true, timeout: 5000, layout: 'topRight' });
        }
        else {
            noty({ text: 'Связь с сервером потеряна. Попытка переподключения ' + attemp.toString() + '...', type: 'error', killer: true, timeout: 5000, layout: 'topRight' });
            setTimeout(function () { _this.checkReconnect(attemp + 1); }, 2000);
        }
    };
    WebSocketClient.prototype.sendCommand = function (command) {
        this.socket.send(JSON.stringify(command));
    };
    return WebSocketClient;
}());
exports.WebSocketClient = WebSocketClient;
//# sourceMappingURL=websocket.service.js.map