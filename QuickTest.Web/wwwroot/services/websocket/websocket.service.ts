﻿import {$WebSocket} from 'angular2-websocket/angular2-websocket';
import {Cookie} from './../../helpers/cookies';
declare var noty;
export class WebSocketClient {
    private socket: $WebSocket;
    private handlers: { [commandName: string]: (ev: string[]) => void } = {};

    public get isConnected(): boolean {
        return this.socket.getReadyState() === 1;
    }

    public attachMessageListenear(commandName: string, callback: (args: string[]) => void) {
        this.handlers[commandName] = callback;
    }

    public detachMessageListenear(commandName: string) {
        this.handlers[commandName] = null;
    }

    public connect(closeHandler?: () => void) {
        this.socket = new $WebSocket('ws://' + location.host + '/Communication?access_token=' + Cookie.load('Auth'),
            undefined, { initialTimeout: 10000, maxTimeout: 30000, reconnectIfNotNormalClose: true });
        this.socket.connect();
        this.socket.onClose((ev: CloseEvent) => {
            if (ev.code != 1000) {
                this.checkReconnect(1);
            } else if (closeHandler) {
                closeHandler();
            }
        });
        this.socket.onMessage((data: Command) => {
            var callback = this.handlers[data.command];
            if (callback) {
                callback.apply(this, [data.args]);
            }
        }, undefined);
    }

    private checkReconnect(attemp: number) {
        if (this.isConnected) {
            noty({ text: 'Связь восстановлена!', type: 'success', killer: true, timeout: 5000, layout: 'topRight' });
        } else if (attemp == 5) {
            noty({ text: 'Не удается соединится с сервером.<br/>Попробуйте перезагрузит страницу. Если ошибка остается, свяжитесь с вашим системным администратором.', type: 'error', killer: true, timeout: 5000, layout: 'topRight' });
        } else {
            noty({ text: 'Связь с сервером потеряна. Попытка переподключения ' + attemp.toString() + '...', type: 'error', killer: true, timeout: 5000, layout: 'topRight' });
            setTimeout(() => { this.checkReconnect(attemp + 1); }, 2000);
        }
    }

    public sendCommand(command: Command) {
        this.socket.send(JSON.stringify(command));
    }
}

export interface Command {
    command: string;
    args: string[];
}