﻿import {DataHandlersModel, DataServiceHandlerIdentifier, DataHandlerModel} from './../../models/data.handlers.model';
import {Http, HTTP_PROVIDERS, Headers} from 'angular2/http';
import {Inject} from 'angular2/core';
import {Cookie} from './../../helpers/cookies';
import {Observer} from 'rxjs/Observer';
import 'rxjs/add/operator/map';

export class AuthService {
    private http: Http;
    private handlersModel: DataHandlersModel;

    constructor( @Inject(Http) http) {
        this.http = http;
        this.handlersModel = new DataHandlersModel();
    }

    public get isAuthenticated(): boolean {
        var token = Cookie.load("Auth");
        return token && token.length > 0;
    }

    public get authType(): AuthType {
        var typeValue = Cookie.load("AuthType");
        if (typeValue && typeValue.length > 0) {
            return AuthType[typeValue];
        }

        return AuthType.NotAuthorized;
    }

    public getStudentToken(id: number, name: string, callback: (success: boolean, reason?: string) => void): void {
        var handler: DataHandlerModel = this.handlersModel.getHandlerByIdentifier(DataServiceHandlerIdentifier.GetStudentToken);
        this.getToken(handler, { id: id, name: name }, AuthType.Student, callback);
    }

    public getTeacherToken(login: string, password: string, callback: (success: boolean, reason?: string) => void): void {
        var handler: DataHandlerModel = this.handlersModel.getHandlerByIdentifier(DataServiceHandlerIdentifier.GetTeacherToken);
        this.getToken(handler, { userName: login, password: password }, AuthType.Teacher, callback);
    }

    private getToken(handler: DataHandlerModel, data: any, type: AuthType, callback: (success: boolean, reason?: string) => void): void {
        var option = new Headers();
        option.append('Content-Type', 'application/json');
        this.http.post(handler.url, JSON.stringify(data), { headers: option })
            .subscribe(res => {
                if (res.status == 200) {
                    var token = res.json();
                    console.log(token);
                    Cookie.save("Auth", 'Bearer ' + token.token, token.expires);
                    Cookie.save("AuthType", type.toString(), token.expires);
                    callback(true);
                } else {
                    callback(false, res.statusText)
                }
            }, res => { callback(false, res.statusText)});
    }
}

export enum AuthType {
    Teacher,
    Student,
    NotAuthorized
}