﻿import {Component, Inject} from 'angular2/core';
import {DataServiceHandlerIdentifier, DataHandlersModel} from './../../models/data.handlers.model';
import {DataService} from './../../services/data/data.service';
import {SessionModel} from './../../models/session.model';
import {bootstrap} from 'angular2/platform/browser';
import {HTTP_PROVIDERS} from 'angular2/http';
declare var jQuery;
declare var noty;
@Component({
    moduleId: module.id,
    selector: 'session-view',
    templateUrl: 'session-view.html',

})
export class SessionViewComponent {
    public urlVars: { [id: string]: string } = {};
    public dataService: DataService;
    public currentSession: SessionModel = new SessionModel();

    constructor( @Inject(DataService) dataService) {
        this.dataService = dataService;
        var hash: string[];
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            this.urlVars[hash[0]] = decodeURIComponent(hash[1].replace(/\+/g, ' '));
        }
        var sessionId = parseInt(this.urlVars['id']);
        this.dataService.performRequest(DataServiceHandlerIdentifier.GetSession, value => {
            if (value.length > 0) {
                noty({ text: value, type: 'error', killer: true, timeout: 5000, layout: 'topRight' });
            } else {
                jQuery.extend(this.currentSession, value);
            }
        }, false, sessionId);
    }
}
//bootstrap(SessionViewComponent, [HTTP_PROVIDERS, DataHandlersModel, DataService])