﻿import {bootstrap} from 'angular2/platform/browser';
import {Component, Inject} from 'angular2/core';
import {ConfigModel} from './../../models/config.model';
import {DataHandlersModel} from './../../models/data.handlers.model';
import {DataService} from './../../services/data/data.service';
import {DataServiceHandlerIdentifier} from './../../models/data.handlers.model';
import {HTTP_PROVIDERS} from 'angular2/http';

@Component({
    moduleId: module.id,
    selector: 'header',
    styleUrls: ['header.css'],
    templateUrl: 'header.html'
})
class HeaderComponent {
    public vm: HeaderComponentViewModel;

    private dataService: DataService;

    constructor( @Inject(DataService) dataService, @Inject(DataHandlersModel) dataHandlerModel) {
        this.dataService = dataService;

        this.vm = new HeaderComponentViewModel();
        this.vm.config.applicationName = "QuickTest";
        this.vm.config.version = "1.0";
        //this.getConfig();
    }

    private getConfig(): void {
        //this.dataService.getData(DataServiceHandlerIdentifier.GetConfig, response => this.vm.config = response);
    }
}

class HeaderComponentViewModel {
    public config: ConfigModel;

    constructor() {
        this.config = new ConfigModel();
    }
}

bootstrap(HeaderComponent, [HTTP_PROVIDERS, DataHandlersModel, DataService]);