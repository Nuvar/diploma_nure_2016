﻿import {Component, ElementRef, Input, EventEmitter, Output} from 'angular2/core';
import {TaskModel} from './../../models/task.model';

declare var jQuery: any;

@Component({
    moduleId: module.id,
    selector: 'task-list',
    templateUrl: 'task-list.html',
    styleUrls: ['task-list.css']
})
export class TaskListComponent {
    private element: ElementRef;
    @Input()
    public viewOnly: boolean = false;
    @Input()
    public sortable: boolean;
    @Input()
    public tasks: Array<TaskModel> = [];
    @Output()
    public onTaskRemoved: EventEmitter<TaskModel> = new EventEmitter();

    constructor(elementRef: ElementRef) {
        this.element = elementRef;
    }
    onRemoveTask(index: number) {
        if (index > -1 && index < this.tasks.length) {
            var removedTask: TaskModel = this.tasks.splice(index, 1)[0];
            this.onTaskRemoved.emit(removedTask);
        }
    }
    addTask(task: TaskModel) {
        if (task) {
            this.tasks.push(task);
        }
    }
}