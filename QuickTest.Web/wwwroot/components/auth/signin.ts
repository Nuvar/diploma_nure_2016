﻿import {Component, Inject} from 'angular2/core';
import {bootstrap} from 'angular2/platform/browser';
import {NgForm, NgControl, Control, Validators} from 'angular2/common';
import {AuthService, AuthType} from './../../services/auth/auth.service';
import {Router} from 'angular2/router';
import {HTTP_PROVIDERS} from 'angular2/http';
import {StudentViewModel, TeacherViewModel} from './../../models/signin.model';
import {SessionLight} from './../../models/session.model';
import {DataService} from './../../services/data/data.service';
import {DataServiceHandlerIdentifier} from './../../models/data.handlers.model';
import {SessionListComponent} from './../session-list/session-list';

@Component({
    moduleId: module.id,
    selector: 'sign-in',
    templateUrl: 'signin.html',
    directives: [SessionListComponent]
})
export class SignInComponent {
    auth: AuthService;
    stname: Control = new Control('', Validators.required);
    tname: Control = new Control('', Validators.required);
    password: Control = new Control('', Validators.required);
    activeSessions: Array<SessionLight> = [];
    currentError: string = '';
    router: Router;
    public student: StudentViewModel = new StudentViewModel();
    public teacher: TeacherViewModel = new TeacherViewModel();

    constructor( @Inject(AuthService) authService, router: Router, @Inject(DataService) data:DataService) {
        this.auth = authService;
        this.router = router;
        if (this.auth.isAuthenticated) {
            var authType = this.auth.authType;
            switch (authType) {
                case AuthType.Teacher: router.navigate(['Home']); break;
            }
        }

        data.performRequest(DataServiceHandlerIdentifier.GetSessions, value => { this.activeSessions = value; }, false);
    }

    teacherSignIn() {
        var reference: SignInComponent = this;
        this.auth.getTeacherToken(this.teacher.UserName, this.teacher.Password,
            function (success: boolean, reason?: string) {
                if (!success) {
                    reference.currentError = reason;
                } else {
                    reference.router.navigate(['Home']);
                }
        });
    }

    studentSignIn() {
        //var reference: SignInComponent = this;
        //this.auth.getStudentToken(this.student.Name, this.student.Group,
        //    function (success: boolean, reason?: string) {
        //        if (!success) {
        //            reference.currentError = reason;
        //        } else {
        //            location.href = '/Home/Trial';
        //        }
        //    });
    }
}
//bootstrap(SignInComponent, [HTTP_PROVIDERS, AuthService]);