﻿import {Component, Input, Output, EventEmitter, Inject, ContentChild} from 'angular2/core';
import {TestModel} from './../../models/test.model';
import {TaskModel} from './../../models/task.model';
import {DataServiceHandlerIdentifier} from './../../models/data.handlers.model';
import {PagerComponent} from './../pager/pager';
import {TestFormComponent} from './../test-form/test-form';
import {ModalComponent, ModalBodyComponent, ModalHeaderComponent, ModalFooterComponent} from './../modal/ng2-bs3-modal';
import {DataService} from './../../services/data/data.service';
declare var noty;
@Component({
    moduleId: module.id,
    selector: 'test-list',
    templateUrl: 'test-list.html',
    directives: [PagerComponent, ModalComponent, ModalBodyComponent, ModalHeaderComponent, ModalFooterComponent, TestFormComponent]
})
export class TestListComponent {
    public tests: Array<TestModel> = [];
    public pageTests: Array<TestModel> = [];
    public dataService: DataService;
    public itemsCount: number;
    public formTitle: string;
    public currentModel: TestModel = new TestModel();
    @ContentChild(PagerComponent) pager: PagerComponent;
    @Input()
    public pickerMode: boolean = false;
    @Output()
    public testPicked = new EventEmitter<TestModel>();

    constructor( @Inject(DataService) dataService) {
        this.dataService = dataService;
        var ref: TestListComponent = this;
        this.dataService.performRequest(DataServiceHandlerIdentifier.GetTests, value => {
    
            ref.tests = value;
            ref.itemsCount = ref.tests.length
            for (var i = 0; i < ref.tests.length && i < 5; i++) {
                ref.pageTests.push(ref.tests[i]);
            }
        }, true);
    }

    refresh() {
        var ref: TestListComponent = this;
        this.dataService.performRequest(DataServiceHandlerIdentifier.GetTests, value => {
            
            ref.tests = value;
            ref.itemsCount = ref.tests.length
            ref.onPageChanged(1);
        }, false);
    }

    onPageChanged(page: number) {
        this.pageTests.length = 0;
        for (var i = (page - 1) * 5; i < this.tests.length && i < page * 5; i++) {
            this.pageTests.push(this.tests[i]);
        }
    }

    resetModel() {
        this.currentModel = new TestModel();
    }

    setModel(model: TestModel) {
        this.currentModel = model;
    }

    saveTest(model: TestModel, modal: ModalComponent) {
        if (model.id > 0) {
            this.dataService.performRequest(DataServiceHandlerIdentifier.UpdateTest, value => {
                if (value.length > 0) {
                    noty({ text: value, type: 'error', killer: true, timeout: 5000, layout: 'topRight' });
                } else {
                    noty({ text: 'Тест успешно обновлен', type: 'success', killer: true, timeout: 5000, layout: 'topRight' });
                }
            }, false, model.id, JSON.stringify(model));
        } else {
            this.dataService.performRequest(DataServiceHandlerIdentifier.SaveTest, value => {
                if (value.length > 0) {
                    noty({ text: value, type: 'error', killer: true, timeout: 5000, layout: 'topRight' });
                } else {
                    model.id = value.id
                    this.tests.push(model);
                    this.onPageChanged(1);
                    this.currentModel = new TestModel();
                    modal.close();
                    noty({ text: 'Тест успешно сохранен', type: 'success', killer: true, timeout: 5000, layout: 'topRight' });
                }
            }, false, undefined, JSON.stringify(model));
        }
    }

    onPickTest(test: TestModel) {
        this.testPicked.emit(test);
    }
}