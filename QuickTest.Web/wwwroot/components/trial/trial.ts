﻿import {Component, ElementRef, Inject} from 'angular2/core';
import {bootstrap} from 'angular2/platform/browser';
import {HTTP_PROVIDERS} from 'angular2/http';
import {DataServiceHandlerIdentifier, DataHandlersModel} from './../../models/data.handlers.model';
import {DataService} from './../../services/data/data.service';
import {AuthService} from './../../services/auth/auth.service';
import {SessionModel, SessionEntity} from './../../models/session.model';
import {WebSocketClient} from './../../services/websocket/websocket.service';
import {TaskModel, MappedTaskModel} from './../../models/task.model';
import {RouteParams, Router} from 'angular2/router';
import {Cookie} from './../../helpers/cookies';

declare var ace;
declare var jQuery;
declare var noty;

@Component({
    moduleId: module.id,
    selector: 'trial',
    templateUrl: 'trial.html',
    styleUrls: ['trial.css']
})
export class TrialComponent {

    private _editor: any;
    private socket: WebSocketClient;
    private dataService: DataService;
    public session: SessionEntity;
    public currentTask: TaskModel = new TaskModel();
    public sessionId: number = 0;
    public auth: AuthService;
    public router: Router;
    public studentsList: Array<StudentOnTest> = [];
    public elem: ElementRef;
    public result: string = '';

    constructor(element: ElementRef,
        @Inject(WebSocketClient) socket,
        @Inject(DataService) dataService,
        @Inject(AuthService) auth,
        params: RouteParams,
        router: Router) {

        this.sessionId = parseInt(params.params['id']);
        this.router = router;
        this.dataService = dataService;
        this.auth = auth;
        this.elem = element;
        
        this.socket = socket;
        this.dataService.performRequest(DataServiceHandlerIdentifier.GetStudentsList, value => {
            this.studentsList = value;
        }, false, this.sessionId);
    }

    connectToSession() {
        var thisRef = this;
        this.dataService.performRequest(DataServiceHandlerIdentifier.GetSession, value => {
            thisRef.session = value;
            thisRef.changeCurrentTask(thisRef, thisRef.session.test.testingTasks[0]);
            thisRef.session.test.testingTasks.splice(0, 1);
            thisRef.socket.attachMessageListenear('SendMessage', (args) => {
                noty({ text: 'Сообщение:' + args[0], type: 'alert', killer: true, timeout: 5000, layout: 'topRight' });
            });
            thisRef.socket.attachMessageListenear('CloseSession', (args) => {
                noty({ text: args[0], type: 'error', killer: true, timeout: 5000, layout: 'topRight' });
                thisRef.socket.detachMessageListenear('CloseSession');
                thisRef.socket.detachMessageListenear('SendMessage');
            });
        }, false, this.sessionId);
        //this.socket.connect();
    }

    selectStudent(student) {
        this.auth.getStudentToken(student.id, student.name, (success, reason) => {
            if (success) {
                this.connectToSession();
            } else {
                noty({ text: "У выбранного студента не осталось свободных попыток", type: 'error', killer: true, timeout: 5000, layout: 'topRight' });
            }
        });
    }

    changeCurrentTask(thisRef: TrialComponent, task: MappedTaskModel) {
        thisRef.dataService.getTask(task.taskID, value => {
            thisRef.currentTask = value;
            var mode: string = 'ace/mode/javascript';
            if (!thisRef._editor) {
                thisRef._editor = ace.edit(thisRef.elem.nativeElement.getElementsByClassName('code-editor')[0]);
            }
            switch (thisRef.currentTask.lang) {
                case 'cs': mode = "ace/mode/csharp"; break;
                case 'py': mode = "ace/mode/python"; break;
                case 'hs': mode = "ace/mode/haskell"; break;
                case 'js': mode = "ace/mode/javascript"; break;
                case 'sq': mode = "ace/mode/sql"; break;
            }

            thisRef._editor.getSession().setMode(mode);
        });
    }

    checkSolution() {
        this.dataService.performRequest(DataServiceHandlerIdentifier.CheckSolution, value => {
            debugger
            this.result = value;
            if (value == 'OK') {
                if (this.session.test.testingTasks.length > 0) {
                    this.changeCurrentTask(this, this.session.test.testingTasks[0]);
                    this.session.test.testingTasks.splice(0, 1);
                } else {
                    noty({ text: "Вы успешно завершили тест!", type: 'success', killer: true, timeout: 5000, layout: 'topRight' });
                    Cookie.remove('Auth');
                    Cookie.remove('AuthType');
                    this.router.navigate(['SignIn']);
                }
            }
        }, false, this.currentTask.id, '"' + encodeURIComponent(this._editor.getValue()) + '"', true);
    }
}
//bootstrap(TrialComponent, [HTTP_PROVIDERS, DataHandlersModel, DataService, WebSocketClient]);
export class StudentOnTest {
    public id: number;
    public name: string;
}