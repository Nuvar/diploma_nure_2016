﻿import {Component, Input, Output, EventEmitter} from 'angular2/core';

@Component({
    moduleId: module.id,
    selector: 'pager',
    templateUrl: 'pager.html'
})
export class PagerComponent {
    @Input()
    public itemsCount: number = 0;

    @Output()
    public pageChanged = new EventEmitter<number>();

    public currentPage: number = 1;
    public pageCount: number;
    public pages: Array<number> = [];

    ngOnInit() {
        this.pageCount = Math.trunc(this.itemsCount / 5) + 1;
        if (this.itemsCount / 5 == 1) this.pageCount--;
        var pagesToDisplay: number = this.pageCount > 5 ? 4 : this.pageCount - 1;
        for (var i = 1; i <= pagesToDisplay; i++) {
            this.pages.push(i);
        }
    }

    onPageChanged(page: number) {
        if (this.pageCount > 10 && page !== this.pageCount - 1) {
            if (page === this.pages[8]) {
                while (page !== this.pages[4] && this.pages[8] !== this.pageCount - 1) {
                    for (var i = 0; i < 8; i++) {
                        this.pages[i] = this.pages[i + 1];
                    }
                    this.pages[8]++;
                }
            } else if (page === this.pages[0] && page !== 1) {
                while (page != this.pages[4] && this.pages[0] !== 1) {
                    for (var i = 8; i > 0; i--) {
                        this.pages[i] == this.pages[i - 1];
                    }
                    this.pages[0]--;
                }
            }
        }
        this.currentPage = page;
        this.pageChanged.emit(page);
    }
}