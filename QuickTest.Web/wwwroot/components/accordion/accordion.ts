﻿import {Component, Input} from 'angular2/core';
import {Helper} from './../../helpers/helper';

@Component({
    moduleId: module.id,
    selector: 'accordion',
    templateUrl: 'accordion.html',
    styleUrls: ['accordion.css']
})
export class AccordionComponent {
    @Input()
    public title: string;

    onToggleAccordion(event) {
        var target = Helper.getEventTarget(event);

        target.classList.toggle('active');
        target.nextElementSibling.classList.toggle("show");
    }
}