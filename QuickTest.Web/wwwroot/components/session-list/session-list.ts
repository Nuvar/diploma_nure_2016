﻿import {bootstrap} from 'angular2/platform/browser';
import {Component, Input, OnChanges} from 'angular2/core';
import {SessionLight} from './../../models/session.model';
import {PagerComponent} from './../pager/pager';
import {SessionListItemComponent} from './../session-list-item/session-list-item';

@Component({
    moduleId: module.id,
    selector: 'session-list',
    templateUrl: 'session-list.html',
    directives: [SessionListItemComponent, PagerComponent]
})
export class SessionListComponent implements OnChanges{
    @Input()
    public sessions: Array<SessionLight>;
    @Input()
    public studentMode: boolean = false;
    public itemsCount: number = 0;
    public pageSessions: Array<SessionLight> = [];

    ngOnInit() {
        if (!this.sessions) return;

        this.itemsCount = this.sessions.length;
        this.onPageChanged(1);
    }
    ngOnChanges(changes) {
        this.onPageChanged(1);
    }
    ngDoCheck() {
        if (this.itemsCount != this.sessions.length) {
            this.itemsCount = this.pageSessions.length;
            this.onPageChanged(1);
        }
    }
    onPageChanged(page: number) {

        this.pageSessions.length = 0;
        for (var i = (page - 1) * 5; i < this.sessions.length && i < page * 5; i++) {
            this.pageSessions.push(this.sessions[i]);
        }
    }

}