﻿import {Component, Input} from 'angular2/core';
import {TestModel} from './../../models/test.model';
import {TaskModel} from './../../models/task.model';
import {TaskListComponent} from './../task-list/task-list';

@Component({
    moduleId: module.id,
    selector: 'test-view-form',
    templateUrl: 'test-view-form.html',
    directives: [TaskListComponent]
})
export class TestViewFormComponent {
    @Input()
    public model: TestModel = new TestModel();
}