﻿import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {TestEditFormComponent} from './test-edit-form';
import {TestViewFormComponent} from './test-view-form';
import {TestModel} from './../../models/test.model';

@Component({
    moduleId: module.id,
    selector: 'test-form',
    directives: [TestEditFormComponent, TestViewFormComponent],
    templateUrl: 'test-form.html'
})
export class TestFormComponent {
    @Input()
    public formMode: string;
    @Input()
    public model: TestModel;
    @Output()
    public testSaved = new EventEmitter<TestModel>();

    onTestSaved(model: TestModel) {
        this.testSaved.emit(model);
    }
}