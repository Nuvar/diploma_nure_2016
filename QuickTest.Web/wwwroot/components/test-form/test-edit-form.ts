﻿import {Component, Input, Output, EventEmitter, Inject} from 'angular2/core';
import {TestModel} from './../../models/test.model';
import {TaskModel, MappedTaskModel} from './../../models/task.model';
import {TaskListComponent} from './../task-list/task-list';
import {NgForm, NgControl, Control, Validators} from 'angular2/common';
import {NumberValidator} from './../../validators/validators';
import {Helper} from './../../helpers/helper';
import {DataService} from './../../services/data/data.service';
import {DataServiceHandlerIdentifier} from './../../models/data.handlers.model';

@Component({
    moduleId: module.id,
    selector: 'test-edit-form',
    templateUrl: 'test-edit-form.html',
    directives: [TaskListComponent]
})
export class TestEditFormComponent {
    @Input()
    public model: TestModel = new TestModel();
    @Output()
    public testSubmitted = new EventEmitter<TestModel>();
    public title: Control = new Control('', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(255)]));
    public estimatedTimeToPass: Control = new Control(15, Validators.compose([Validators.required, NumberValidator.isFloat]));


    public allTasks: Array<TaskModel> = [];
    public avaliableTasks: Array<TaskModel> = [];
    public selectedTasks: Array<TaskModel> = [];
    public tasksValid: boolean = true;
    public dataService: DataService;

    onSubmit(selectedTasks: Array<TaskModel>) {
        if (selectedTasks.length === 0) {
            this.tasksValid = false;
            return;
        }

        this.tasksValid = true;
        this.model.testingTasks.length = 0;
        for (var i: number = 0; i < selectedTasks.length; i++) {
            var taskMap = new MappedTaskModel();
            taskMap.taskID = selectedTasks[i].id;
            taskMap.testId = this.model.id;
            this.model.testingTasks.push(taskMap);
        }
        this.testSubmitted.emit(this.model);
    }

    ngOnInit() {
        this.dataService.performRequest(DataServiceHandlerIdentifier.GetTasks, value => {
            this.allTasks = value;
            for (var i = 0; i < this.allTasks.length; i++) {
                this.avaliableTasks.push(this.allTasks[i]);
            }

            for (var i = 0; i < this.model.testingTasks.length; i++) {
                var task = this.model.testingTasks[i];
                var index = Helper.getIndexOf(this.avaliableTasks, function (item) {
                    return item.id === task.taskID;
                });
                var taskInfo: TaskModel = this.dataService.getFromCacheTask(task.taskID);
                this.selectedTasks.push(taskInfo);
                this.avaliableTasks.splice(index, 1);
            }
        }, true);
    }

    constructor( @Inject(DataService) dataService: DataService) {
        this.dataService = dataService;
    }
}