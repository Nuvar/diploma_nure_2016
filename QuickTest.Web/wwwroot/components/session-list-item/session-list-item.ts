﻿import {bootstrap} from 'angular2/platform/browser';
import {Component, Input} from 'angular2/core';
import {SessionLight} from './../../models/session.model';
import {Router} from 'angular2/router';

@Component({
    selector: 'session-list-item',
    moduleId: module.id,
    templateUrl: 'session-list-item.html'
})
export class SessionListItemComponent {
    @Input()
    public session: SessionLight;
    public router: Router;
    @Input()
    public studentMode: boolean = false;
    constructor(router: Router) {
        this.router = router;
    }

    onCloseSession() {
        this.session.isOpened = false;
    }

    onDisplaySession() {
        this.router.navigate(['SessionView', { id: this.session.id }]);
    }

    goToTrial() {
        this.router.navigate(['Trial', { id: this.session.id }]);
    }
}