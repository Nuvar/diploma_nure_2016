﻿import {RouteConfig, Route, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, LocationStrategy, HashLocationStrategy, RouterOutlet} from 'angular2/router';
import {Component, provide} from 'angular2/core';
import {bootstrap} from 'angular2/platform/browser';
import {HomeComponent} from './home/home';
import {SignInComponent} from './auth/signin';
import {TrialComponent} from './trial/trial';
import {SessionViewComponent} from './session-view/session-view';
import {AuthService, AuthType} from './../services/auth/auth.service';
import {DataService} from './../services/data/data.service';
import {DataServiceHandlerIdentifier, DataHandlersModel} from './../models/data.handlers.model';
import {WebSocketClient} from './../services/websocket/websocket.service';
import {HTTP_PROVIDERS} from 'angular2/http';

@Component({
    moduleId: module.id,
    selector: 'app',
    directives: [RouterOutlet],
    template: '<router-outlet></router-outlet>'
})
@RouteConfig([
    { path: '/home', name: 'Home', component: HomeComponent },
    { path: '/', name: 'SignIn', component: SignInComponent, useAsDefault: true },
    { path: '/trial', name: 'Trial', component: TrialComponent },
    { path: '/sessionView/:id', name: 'SessionView', component: SessionViewComponent }
])
class AppComponent {
}

bootstrap(AppComponent, [
    ROUTER_PROVIDERS,
    provide(LocationStrategy,
        { useClass: HashLocationStrategy }),
    HTTP_PROVIDERS, AuthService, DataHandlersModel, DataService, WebSocketClient
]);