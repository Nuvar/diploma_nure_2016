﻿import {TestListComponent} from './../test-list/test-list';
import {Component, Inject} from 'angular2/core';
import {bootstrap} from 'angular2/platform/browser';
import {SessionListComponent} from './../session-list/session-list';
import {TaskListComponent} from './../task-list/task-list';
import {TaskModel} from './../../models/task.model';
import {Tab, Tabs} from './../tab-panel/tabs';
import {TestModel} from './../../models/test.model';
import {Router} from 'angular2/router';
import {HTTP_PROVIDERS} from 'angular2/http';
import {SessionModel, SessionLight} from './../../models/session.model';
import {ModalComponent, ModalBodyComponent, ModalHeaderComponent, ModalFooterComponent} from './../modal/ng2-bs3-modal';
import {DataService} from './../../services/data/data.service';
import {DataServiceHandlerIdentifier, DataHandlersModel} from './../../models/data.handlers.model';
import {NgForm, NgControl, Control, Validators} from 'angular2/common';
import {NumberValidator} from './../../validators/validators';
declare var currentUserName: any;
declare var noty: any;
declare var jQuery: any;
@Component({
    selector: 'home',
    moduleId: module.id,
    templateUrl: 'home.html',
    styleUrls: ['home.css'],
    directives: [SessionListComponent, TaskListComponent, Tabs, Tab, TestListComponent, ModalComponent, ModalBodyComponent, ModalHeaderComponent, ModalFooterComponent]
})
export class HomeComponent {
    public sessionModel: SessionModel = new SessionModel();
    public sessionValid: boolean = true;
    public dataService: DataService;
    public testTime: Control = new Control(15, Validators.compose([Validators.required, NumberValidator.isFloat]));
    public activeSessions: Array<SessionLight> = [];
    public completedSessions: Array<SessionLight> = [];
    public router: Router;
    constructor( @Inject(DataService) dataService, router: Router) {
        this.dataService = dataService;
        this.router = router;
        this.refresh();
    }

    refresh() {
        this.dataService.performRequest(DataServiceHandlerIdentifier.GetSessions, value => {
            var sessions: Array<SessionLight> = value;
            this.activeSessions.length = 0;
            this.completedSessions.length = 0;
            for (var i = 0; i < sessions.length; i++) {
                var session: SessionLight = sessions[i];
                debugger
                session.studentsCount = session.studentAttemps.length;
                if (session.isOpened) {
                    this.activeSessions.push(session);
                } else {
                    this.completedSessions.push(session);
                }
            }
        }, false);
    }

    addSession() {
        if (!this.sessionModel.test) {
            this.sessionValid = false;
            return;
        }

        this.sessionValid = true;
        this.sessionModel.startTime = new Date(Date.now());
        this.sessionModel.isOpened = true;

        this.dataService.performRequest(DataServiceHandlerIdentifier.CreateSession, value => {
            if (value.length > 0) {
                noty({ text: value, type: 'error', killer: true, timeout: 5000, layout: 'topRight' });
            } else {
                this.router.navigate(['SessionView', { id: value.id }]);
            }
        }, false, undefined, this.sessionModel.toJson());
    }

    onTestSelected(test: TestModel) {
        this.dataService.performRequest(DataServiceHandlerIdentifier.GetTest, value => {
            this.sessionModel.test = value;
        }, false, test.id);
    }
}
//bootstrap(HomeComponent, [HTTP_PROVIDERS, DataHandlersModel, DataService]);