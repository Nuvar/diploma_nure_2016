﻿import {ValidationResult} from './validators';
import {Control} from 'angular2/common';

export class NumberValidator {
    static isFloat(control: Control): ValidationResult {
        if (isNaN(parseFloat(control.value))) {
            return {
                'isFloat': true
            };
        }

        return null;
    }
}