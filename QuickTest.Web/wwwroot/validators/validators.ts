﻿export * from './number.validator';

export interface ValidationResult {
    [key: string]: boolean;
}