﻿import {MappedTaskModel} from './task.model';

export class TestModel {
    public id: number = 0;
    public title: string = '';
    public estimatedTimeToPass: any = 0;
    public testingTasks: Array<MappedTaskModel> = [];
    
}