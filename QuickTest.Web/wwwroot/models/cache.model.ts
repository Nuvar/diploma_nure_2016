﻿import {DataServiceHandlerIdentifier} from './data.handlers.model';

export class CacheModel {
    public id: DataServiceHandlerIdentifier;
    public chachedObject: any;

    constructor(id: DataServiceHandlerIdentifier, obj: any) {
        this.id = id;
        this.chachedObject = obj;
    }
}