﻿import {TestModel} from './test.model';

export class SessionLight {
    public id: number;
    public name: string;
    public studentsCount: number;
    public studentAttemps: Array<any> = [];
    public isOpened: boolean;

    constructor() {
        
    }
}

export class SessionModel {
    public id: number;
    public name: string;
    public startTime: Date;
    get startTimeString(): string {
        return this.startTime.toISOString();
    }

    public endTime: Date;
    public endTimeString(): string {
        return this.endTime.toISOString();
    }

    public testTime: any = 0;
    public isOpened: boolean;
    public test: TestModel;

    public students: string;

    public toJson(): string {
        var obj = {
            name: this.name,
            startTime: this.startTimeString,
            endTime: this.endTimeString,
            testTime: this.testTime,
            isOpened: this.isOpened,
            testId: this.test.id,
            students: this.students
        };

        return JSON.stringify(obj);
    }
}

export class SessionEntity {
    public id: number;
    public name: string;
    public test: TestModel;
    public testTime: string;
    public get testTimeMiliseconds(): number {
        var splitted = this.testTime.split(':');
        var lastIndex = splitted.length - 1;
        var modifier: number = 1000;
        var result: number = 0;
        for (var i = lastIndex; i >= 0; i++) {
            var val: number = parseInt(splitted[i]);
            result += val * modifier;
            modifier *= 60;
        }
        return result;
    }
}