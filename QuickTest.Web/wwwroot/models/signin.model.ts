﻿export class TeacherViewModel {
    public UserName: string;
    public Password: string;
}

export class StudentViewModel {
    public Name: string;
    public Group: string;
}