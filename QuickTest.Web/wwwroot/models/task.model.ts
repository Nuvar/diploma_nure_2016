﻿export class TaskModel {
    public id: number =0;
    public title: string = '';
    public attr: string = '';
    public lang: string = '';
    public cond: string = '';
    public view: string = '';
    public hint: string = '';
    public code: string = '';
}

export class MappedTaskModel {
    public taskID: number;
    public testId: number;
}