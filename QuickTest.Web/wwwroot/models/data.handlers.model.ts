﻿export class DataHandlersModel {
    public handlers: Array<DataHandlerModel> = [];

    constructor() {
        this.addApplicationHandlers();
    }

    public addApplicationHandlers() {
        this.handlers.push({ identifier: DataServiceHandlerIdentifier.GetSessions, url: '/api/TestSession', method: HttpMethods.GET });
        this.handlers.push({ identifier: DataServiceHandlerIdentifier.GetSession, url: '/api/TestSession/', method: HttpMethods.GET });
        this.handlers.push({ identifier: DataServiceHandlerIdentifier.GetTasks, url: '/api/Task', method: HttpMethods.GET });
        this.handlers.push({ identifier: DataServiceHandlerIdentifier.GetTask, url: '/api/Task/Info/', method: HttpMethods.GET });
        this.handlers.push({ identifier: DataServiceHandlerIdentifier.GetTests, url: '/api/Test', method: HttpMethods.GET });
        this.handlers.push({ identifier: DataServiceHandlerIdentifier.GetTeacherToken, url: '/api/Teacher/Token', method: HttpMethods.POST });
        this.handlers.push({ identifier: DataServiceHandlerIdentifier.GetStudentToken, url: '/api/Student/Token', method: HttpMethods.POST });
        this.handlers.push({ identifier: DataServiceHandlerIdentifier.SaveTest, url: '/api/Test', method: HttpMethods.POST });
        this.handlers.push({ identifier: DataServiceHandlerIdentifier.UpdateTest, url: '/api/Test/', method: HttpMethods.PUT });
        this.handlers.push({ identifier: DataServiceHandlerIdentifier.GetTest, url: '/api/Test/', method: HttpMethods.GET });
        this.handlers.push({ identifier: DataServiceHandlerIdentifier.CreateSession, url: '/api/TestSession', method: HttpMethods.POST });
        this.handlers.push({ identifier: DataServiceHandlerIdentifier.GetStudentsList, url: '/api/TestSession/StudentsList/', method: HttpMethods.GET });
        this.handlers.push({ identifier: DataServiceHandlerIdentifier.StartSession, url: '/api/TestSession/Start/', method: HttpMethods.GET });
        this.handlers.push({ identifier: DataServiceHandlerIdentifier.CheckSolution, url: '/api/Check/', method: HttpMethods.POST });
    }

    public getHandlerByIdentifier(identifier: DataServiceHandlerIdentifier): DataHandlerModel {
        for (var i = 0, length = this.handlers.length; i < length; i++) {
            var currentHandler = this.handlers[i];
            if (currentHandler.identifier == identifier) {
                return currentHandler;
            }
        }

        return null;
    }
}

export enum DataServiceHandlerIdentifier {
    GetSessions,
    GetSession,
    GetTasks,
    GetTask,
    GetTests,
    GetTest,
    CreateSession,
    UpdateTest,
    SaveTest,
    GetStudentToken,
    GetTeacherToken,
    GetStudentsList,
    StartSession,
    CheckSolution
}

export enum HttpMethods {
    GET,
    POST,
    PUT,
    DELETE
}

export class DataHandlerModel {
    public identifier: DataServiceHandlerIdentifier;
    public url: string;
    public method: HttpMethods;
}