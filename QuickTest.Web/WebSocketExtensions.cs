﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QuickTest.Web
{
    public static class WebSocketExtensions
    {
        public static Task SendTextAsync(this WebSocket socket, string text)
        {
            var token = CancellationToken.None;
            var type = WebSocketMessageType.Text;
            var data = Encoding.UTF8.GetBytes(text);
            var buffer = new ArraySegment<byte>(data);

            return socket.SendAsync(buffer, type, true, token);
        }
    }
}
