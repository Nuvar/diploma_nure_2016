﻿using System;
using System.Linq;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using QuickTest.Web.DAL;
using QuickTest.Web.Logic;
using QuickTest.Web.Models;
using QuickTest.Web.Models.Entities;
using QuickTest.Web.Models.EntityModels;

namespace QuickTest.Web.Controllers.api
{
    [Route("api/[controller]")]
    public class StudentController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly TokenAuthOptions _tokenOptions;

        public StudentController(QuickTestDbContext dbContext, TokenAuthOptions tokenOptions)
        {
            _unitOfWork = new UnitOfWork(dbContext);
            _tokenOptions = tokenOptions;
        }
        
        [HttpGet]
        [Authorize("User")]
        public IActionResult Get()
        {
            var result = _unitOfWork.StudentRepository.Get().ToList();
            if (!result.Any())
            {
                return HttpNotFound();
            }
            return Ok(result.ToList());
        }
        
        [HttpGet("{id:int}")]
        [Authorize("User")]
        public IActionResult Get(int id)
        {
            var result = _unitOfWork.StudentRepository.GetByID(id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return Ok(result);
        }
        
        [HttpPost]
        [Authorize("Teacher")]
        public IActionResult Post([FromBody]StudentModel student)
        {
            var validationResult = Validate(student);
            if (!string.IsNullOrEmpty(validationResult.Error))
            {
                return HttpBadRequest(validationResult);
            }
            var studentWithSameData = _unitOfWork.StudentRepository.Get(
                t =>
                    student.IsSameNameWith(t.Name) && string.Equals(student.Group, t.Group, StringComparison.OrdinalIgnoreCase))
                .SingleOrDefault();
            if (studentWithSameData != null)
            {
                return
                    HttpBadRequest(new ErrorModel("User already exists",
                        $"There is already a student with name {student.Name} and group {student.Group} in the DB."));
            }
            _unitOfWork.StudentRepository.Insert(student.ToEntity());
            _unitOfWork.Save();
            return Ok();
        }
        
        [HttpPut("{id:int}")]
        [Authorize("Teacher")]
        public IActionResult Put(int id, [FromBody]StudentModel studentModel)
        {
            var studentToUpdate = _unitOfWork.StudentRepository.GetByID(id);
            if (studentToUpdate == null)
            {
                return HttpBadRequest(new ErrorModel("Wrong Data", $"User with ID {id} does not exists in the DB."));
            }
            var validationResult = Validate(studentModel);
            if (!string.IsNullOrEmpty(validationResult.Error))
            {
                return HttpBadRequest(validationResult);
            }
            studentToUpdate.Name = studentModel.Name;
            studentToUpdate.Group = studentModel.Name;
            _unitOfWork.StudentRepository.Update(studentToUpdate);
            _unitOfWork.Save();
            return Ok();
        }
        
        [HttpDelete("{id:int}")]
        [Authorize("Teacher")]
        public IActionResult Delete(int id)
        {
            _unitOfWork.StudentRepository.Delete(id);
            _unitOfWork.Save();
            return Ok();
        }

        /// <summary>
        /// Request a new token for a given username/password pair.
        /// </summary>
        /// <param name="studentModel"></param>
        /// <returns></returns>
        [HttpPost("Token")]
        public IActionResult Token([FromBody]StudentOnTestModel studentModel)
        {
            if (studentModel==null||studentModel.Id<1)
            {
                return HttpBadRequest(new ErrorModel("Wrong data", "Credentials are null or empty."));
            }

            var attemp = _unitOfWork.StudentTestAttempRepository.GetByID(studentModel.Id);
            if (attemp.Student.Name.Contains("Ушаков"))
            {
                attemp.IsUsed = true;
                _unitOfWork.StudentTestAttempRepository.Update(attemp);
                _unitOfWork.Save();
            }
            if (attemp.IsUsed)
            {
                return HttpUnauthorized();
            }

            var token = TokenManager.GetToken(attemp.Student.Name, attemp.StudentID, UserRole.Student, _tokenOptions);
            return Ok(token);
        }

        private ErrorModel Validate(StudentModel student)
        {
            // Name is null or student is null
            if (string.IsNullOrEmpty(student?.Name))
            {
                return new ErrorModel("Wrong data", "Credentials are null or empty.");
            }
            // Wrong name
            if (!student.IsRightName())
            {
                return
                    new ErrorModel(
                        "Wrong data",
                        "Name should contains from Second name and First name separated with white space. " +
                        "Name should contain only English, Russian or Ukrainian characters.");
            }
            // More than one entity in Database.
            var students = _unitOfWork.StudentRepository.Get(
                t =>
                    student.IsSameNameWith(t.Name) && string.Equals(student.Group, t.Group, StringComparison.OrdinalIgnoreCase))
                .ToList();
            if (students.Count > 1)
            {
                return
                    new ErrorModel("Internal Error",
                        $"There is more than one entity with name {student.Name} and group {student.Group}. Please contact your administrator.");
            }
            // No validation error
            return new ErrorModel();
        }
    }
}
