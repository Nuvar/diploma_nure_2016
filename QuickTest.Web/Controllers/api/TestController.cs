﻿using System;
using System.Linq;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using QuickTest.Web.DAL;
using QuickTest.Web.Models;
using QuickTest.Web.Models.Entities;
using QuickTest.Web.Models.EntityModels;

namespace QuickTest.Web.Controllers.api
{
    [Route("api/[controller]")]
    public class TestController : Controller
    {
        private readonly UnitOfWork _unitOfWork;

        public TestController(QuickTestDbContext dbContext)
        {
            _unitOfWork = new UnitOfWork(dbContext);
        }

        [HttpGet]
        [Authorize("Teacher")]
        public IActionResult Get()
        {
            var result = _unitOfWork.TestRepository.Get().ToList();
            if (!result.Any())
            {
                return HttpNotFound();
            }
            return Ok(result.ToList());
        }

        [HttpGet("{id:int}")]
        [Authorize("User")]
        public IActionResult Get(int id)
        {
            var result = _unitOfWork.TestRepository.GetByID(id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return Ok(result);
        }

        [HttpPost]
        [Authorize("Teacher")]
        public IActionResult Post([FromBody]TestModel test)
        {
            var validationResult = Validate(test);
            if (!string.IsNullOrEmpty(validationResult.Error))
            {
                return HttpBadRequest(validationResult);
            }
            var testToCreate = test.ToEntity();
            testToCreate.AuthorID = Convert.ToInt32(User.Claims.Single(c => string.Equals(c.Type, "EntityID")).Value);
            _unitOfWork.TestRepository.Insert(testToCreate);
            _unitOfWork.Save();
            return Ok(testToCreate);
        }

        [HttpPut("{id:int}")]
        [Authorize("Teacher")]
        public IActionResult Put(int id, [FromBody]TestModel test)
        {
            var testToUpdate = _unitOfWork.TestRepository.GetByID(id);
            if (testToUpdate == null)
            {
                return HttpBadRequest(new ErrorModel("Wrong Data", $"Test with ID {id} does not exists in the DB."));
            }
            var validationResult = Validate(test);
            if (!string.IsNullOrEmpty(validationResult.Error))
            {
                return HttpBadRequest(validationResult);
            }
            testToUpdate.EstimatedTimeToPass = TimeSpan.FromMinutes(test.EstimatedTimeToPass);
            testToUpdate.Title = test.Title;
            _unitOfWork.TestRepository.Update(testToUpdate);
            _unitOfWork.Save();
            return Ok();
        }

        [HttpDelete("{id:int}")]
        [Authorize("Teacher")]
        public IActionResult Delete(int id)
        {
            _unitOfWork.TestRepository.Delete(id);
            _unitOfWork.Save();
            return Ok();
        }

        private ErrorModel Validate(TestModel test)
        {
            // Test or Title is null
            if (string.IsNullOrEmpty(test?.Title))
            {
                return new ErrorModel("Wrong data", "Model is null or Title is null.");
            }
            // Test with same name exists
            if (_unitOfWork.TestRepository.Get(t => string.Equals(t.Title, test.Title)).Any())
            {
                return new ErrorModel("Wrong data", "Test with the same title already exists in the DB.");
            }
            // No validation error
            return new ErrorModel();
        }
    }
}
