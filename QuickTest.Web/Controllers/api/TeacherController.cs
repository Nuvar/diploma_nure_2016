﻿using System;
using System.Linq;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using Newtonsoft.Json.Linq;
using QuickTest.Web.DAL;
using QuickTest.Web.Logic;
using QuickTest.Web.Models;
using QuickTest.Web.Models.Entities;
using QuickTest.Web.Models.EntityModels;

namespace QuickTest.Web.Controllers.api
{
    [Route("api/[controller]")]
    public class TeacherController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly TokenAuthOptions _tokenOptions;

        public TeacherController(QuickTestDbContext dbContext, TokenAuthOptions tokenOptions)
        {
            _unitOfWork = new UnitOfWork(dbContext);
            _tokenOptions = tokenOptions;
        }

        [HttpGet]
        [Authorize("Teacher")]
        public IActionResult Get()
        {
            var result = _unitOfWork.TeacherRepository.Get().ToList();
            if (!result.Any())
            {
                return HttpNotFound();
            }
            return Ok(result.Select(t => new TeacherModel(t)));
        }

        [HttpGet("{id:int}")]
        [Authorize("Teacher")]
        public IActionResult Get(int id)
        {
            var result = _unitOfWork.TeacherRepository.GetByID(id);

            if (result == null)
            {
                return HttpNotFound();
            }

            if (!string.Equals(HttpContext.User.Identity.Name, result.UserName, StringComparison.OrdinalIgnoreCase))
            {
                return HttpUnauthorized();
            }

            return Ok(result);
        }

        [HttpGet("{id:int}/info")]
        [Authorize("User")]
        public IActionResult GetInfo(int id)
        {
            var result = _unitOfWork.TeacherRepository.GetByID(id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return Ok(new TeacherModel(result));
        }

        /// <summary>
        /// Request a new token for a given username/password pair.
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        [HttpPost("Token")]
        public IActionResult Token([FromBody] TeacherCredentials credentials)
        {
            var validationResult = Validate(credentials);
            if (!string.IsNullOrEmpty(validationResult.Error))
            {
                return HttpBadRequest(validationResult);
            }

            var teacher = _unitOfWork.TeacherRepository.Get(
                t =>
                    string.Equals(t.UserName, credentials.UserName, StringComparison.OrdinalIgnoreCase))
                .SingleOrDefault();

            if (teacher == null)
            {
                // Try to login to Judge. If success - then create Teacher in DB
                var judgeTokenResult = JudgeRequest.GetJudgeAuthToken(credentials.UserName, credentials.Password);
                if (!judgeTokenResult.Success)
                {
                    return HttpNotFound(new ErrorModel("User not found.", "User not found."));
                }
                var judgeToken = judgeTokenResult.Result;

                // User exists in Judge, but not exists in QuickTest.
                // Getting Teacher Info from Judge and adding Teacher and save token
                var userInfoResult = JudgeRequest.GetJudgeUserInfo(judgeToken.AccessToken);

                if (!userInfoResult.Success)
                {
                    return
                        HttpBadRequest(new ErrorModel("Internal Error",
                            "Can not get UserInfo from Judge server. Please contact your administrator."));
                }

                // Create Teacher and Token
                var teacherToCreate = userInfoResult.Result.ToEntity();
                teacherToCreate.Password = credentials.Password;
                var judgeTokenToCreate = judgeToken.ToEntity();
                _unitOfWork.InsertTeacherWithJudgeToken(teacherToCreate, judgeTokenToCreate);

                teacher =
                    _unitOfWork.TeacherRepository.Get(
                        t => string.Equals(t.UserName, teacherToCreate.UserName, StringComparison.OrdinalIgnoreCase))
                        .FirstOrDefault();
                if (teacher == null)
                {
                    return
                        HttpBadRequest(new ErrorModel("Internal Error",
                            "Problems with saving teacher to database. Please contact your administrator."));
                }
            }

            if (!string.Equals(teacher.Password, credentials.Password, StringComparison.Ordinal))
            {
                return HttpNotFound(new ErrorModel("User not found", "User not found."));
            }

            var token = TokenManager.GetToken(teacher.UserName, teacher.ID, UserRole.Teacher, _tokenOptions);
            return Ok(token);
        }

        private ErrorModel Validate(TeacherCredentials credentials)
        {
            // Data is null or missing
            if (credentials == null || string.IsNullOrEmpty(credentials.UserName) ||
                string.IsNullOrEmpty(credentials.Password))
            {
                return new ErrorModel("Wrong data", "Credentials are null or empty.");
            }
            // More than one entity in Database.
            var teachers = _unitOfWork.TeacherRepository.Get(
                t =>
                    string.Equals(t.UserName, credentials.UserName, StringComparison.OrdinalIgnoreCase))
                .ToList();
            if (teachers.Count > 1)
            {
                return
                    new ErrorModel("Internal Error",
                        $"There is more than one Tecaher with name {credentials.UserName}. Please contact your administrator.");
            }
            // No validation error
            return new ErrorModel();
        }
    }
}
