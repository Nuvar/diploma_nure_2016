﻿using System;
using System.Linq;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using QuickTest.Web.DAL;
using QuickTest.Web.Models;
using QuickTest.Web.Models.EntityModels;

namespace QuickTest.Web.Controllers.api
{
    [Route("api/[controller]")]
    public class TestingTaskController : Controller
    {
        private readonly UnitOfWork _unitOfWork;

        public TestingTaskController(QuickTestDbContext dbContext)
        {
            _unitOfWork = new UnitOfWork(dbContext);
        }

        [HttpGet]
        [Authorize("User")]
        public IActionResult Get()
        {
            var result = _unitOfWork.TestingTaskRepository.Get().ToList();
            if (!result.Any())
            {
                return HttpNotFound();
            }
            return Ok(result.ToList());
        }

        [HttpGet("{id:int}")]
        [Authorize("User")]
        public IActionResult Get(int id)
        {
            var result = _unitOfWork.TestingTaskRepository.GetByID(id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return Ok(result);
        }

        [HttpPost]
        [Authorize("Teacher")]
        public IActionResult Post([FromBody]TestingTaskModel testingTask)
        {
            var validationResult = Validate(testingTask);
            if (!string.IsNullOrEmpty(validationResult.Error))
            {
                return HttpBadRequest(validationResult);
            }
            _unitOfWork.TestingTaskRepository.Insert(testingTask.ToEntity());
            _unitOfWork.Save();
            return Ok();
        }

        [HttpDelete("{id:int}")]
        [Authorize("Teacher")]
        public IActionResult Delete(int id)
        {
            var entityToDelete = _unitOfWork.TestingTaskRepository.GetByID(id);
            if (entityToDelete.Test.AuthorID != Convert.ToInt32(User.Claims.Single(c => string.Equals(c.Type, "EntityID")).Value))
            {
                return HttpUnauthorized();
            }
            _unitOfWork.TestingTaskRepository.Delete(id);
            _unitOfWork.Save();
            return Ok();
        }

        private ErrorModel Validate(TestingTaskModel testingTask)
        {
            // Test or Title is null
            if (testingTask == null)
            {
                return new ErrorModel("Wrong data", "Model is null or Title is null.");
            }
            // Test not Exists
            if (_unitOfWork.TestRepository.GetByID(testingTask.TestID) == null)
            {
                return new ErrorModel("Wrong data", $"Test with ID {testingTask.TestID} does not exists in the DB.");
            }
            // No validation error
            return new ErrorModel();
        }
    }
}
