﻿using System;
using System.Linq;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using QuickTest.Web.DAL;
using QuickTest.Web.Logic;
using QuickTest.Web.Models;

namespace QuickTest.Web.Controllers.api
{
    [Route("api/[controller]")]
    public class TaskController : Controller
    {
        private readonly UnitOfWork _unitOfWork;

        public TaskController(QuickTestDbContext dbContext)
        {
            _unitOfWork = new UnitOfWork(dbContext);
            
        }
        
        [HttpGet]
        [Authorize("Teacher")]
        public IActionResult Get()
        {
            var tokenResult = GetUserJudgeToken();
            if (!tokenResult.Success)
            {
                return HttpBadRequest(tokenResult.Error);
            }
            var judgeResult = JudgeRequest.GetTasks(tokenResult.Result);
            if (!judgeResult.Success)
            {
                return HttpBadRequest(judgeResult.Error);
            }
            return Ok(judgeResult.Result);
        }

        [HttpGet("{id:int}")]
        [Authorize("Teacher")]
        public IActionResult Get(int id)
        {
            var tokenResult = GetUserJudgeToken();
            if (!tokenResult.Success)
            {
                return HttpBadRequest(tokenResult.Error);
            }
            var judgeResult = JudgeRequest.GetFullTaskByID(id, tokenResult.Result);
            if (!judgeResult.Success)
            {
                return HttpBadRequest(judgeResult.Error);
            }
            return Ok(judgeResult.Result);
        }

        [HttpGet("Info/{id:int}")]
        [Authorize("User")]
        public IActionResult GetInfo(int id)
        {
            var judgeResult = JudgeRequest.GetTaskInfoByID(id);
            if (!judgeResult.Success)
            {
                return HttpBadRequest(judgeResult.Error);
            }
            return Ok(judgeResult.Result);
        }

        [HttpGet("{id:int}/solution")]
        [Authorize("Teacher")]
        public IActionResult GetSolution(int id)
        {
            var tokenResult = GetUserJudgeToken();
            if (!tokenResult.Success)
            {
                return HttpBadRequest(tokenResult.Error);
            }
            var judgeResult = JudgeRequest.GetAuthorTaskSolution(id, tokenResult.Result);
            if (!judgeResult.Success)
            {
                return HttpBadRequest(judgeResult.Error);
            }
            return Ok(judgeResult.Result);
        }

        private RequestResult<string> GetUserJudgeToken()
        {
            var thisTeacher = _unitOfWork.TeacherRepository.GetByID(
                Convert.ToInt32(User.Claims.Single(c => string.Equals(c.Type, "EntityID")).Value));
            var token = thisTeacher.JudgeToken;
            if (token.Expires.ToUniversalTime().CompareTo(DateTime.UtcNow) < 0)
            {
                return new RequestResult<string>(200, token.AccessToken);
            }

            // Need to update token
            // Get token
            var judgeTokenResult = JudgeRequest.GetJudgeAuthToken(thisTeacher.UserName, thisTeacher.Password);
            if (!judgeTokenResult.Success)
            {
                return new RequestResult<string>(judgeTokenResult.StatusCode,
                    new ErrorModel("Internal Error", "Error connecting to JudgeServer"));
            }
            var judgeToken = judgeTokenResult.Result;

            // Update Token
            token.AccessToken = judgeToken.AccessToken;
            token.Expires = judgeToken.Expires;
            token.ExpiresIn = judgeToken.ExpiresIn;
            token.Issued = judgeToken.Issued;
            token.TokenType = judgeToken.TokenType;
            token.UserName = judgeToken.UserName;

            _unitOfWork.JudgeTokenRepository.Update(token);
            _unitOfWork.Save();
            return new RequestResult<string>(judgeTokenResult.StatusCode, token.AccessToken);
        }
    }
}
