﻿using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using QuickTest.Web.Logic;
using Microsoft.Framework.WebEncoders;

namespace QuickTest.Web.Controllers.api
{
    [Route("api/[controller]")]
    [Authorize("User")]
    public class CheckController : Controller
    {
        [HttpPost("{id:int}")]
        public IActionResult Post(int id, [FromBody]string solution)
        {
            string parsedSolution = System.Net.WebUtility.UrlDecode(solution);
           var judgeResult = JudgeRequest.CheckTask(id, parsedSolution);
            if (!judgeResult.Success)
            {
                return HttpBadRequest(judgeResult.Error);
            }

            return Ok(judgeResult.Result);
        }

        [HttpPut("{language}")]
        public IActionResult Put(string language, [FromBody]string code)
        {
            var judgeResult = JudgeRequest.CheckCode(language, code);
            if (!judgeResult.Success)
            {
                return HttpBadRequest(judgeResult.Error);
            }
            return Ok(judgeResult.Result);
        }
    }
}
