﻿using System;
using System.Linq;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using QuickTest.Web.DAL;
using QuickTest.Web.Models;
using QuickTest.Web.Models.Entities;
using QuickTest.Web.Models.EntityModels;

namespace QuickTest.Web.Controllers.api
{
    [Route("api/[controller]")]
    public class TaskSolutionController : Controller
    {
        private readonly UnitOfWork _unitOfWork;

        public TaskSolutionController(QuickTestDbContext dbContext)
        {
            _unitOfWork = new UnitOfWork(dbContext);
        }

        [HttpGet]
        [Authorize("Teacher")]
        public IActionResult Get()
        {
            var result = _unitOfWork.TaskSolutionRepository.Get().ToList();
            if (!result.Any())
            {
                return HttpNotFound();
            }
            return Ok(result.ToList());
        }

        [HttpGet("{id:int}")]
        [Authorize("Teacher")]
        public IActionResult Get(int id)
        {
            var result = _unitOfWork.TaskSolutionRepository.GetByID(id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return Ok(result);
        }

        [HttpPost]
        [Authorize("Student")]
        public IActionResult Post([FromBody]TaskSolutionModel taskSolution)
        {
            var validationResult = Validate(taskSolution);
            if (!string.IsNullOrEmpty(validationResult.Error))
            {
                return HttpBadRequest(validationResult);
            }
            var attemp = _unitOfWork.StudentTestAttempRepository.GetByID(taskSolution.StudentTestAttempID);
            var thisStudentId = Convert.ToInt32(User.Claims.Single(c => string.Equals(c.Type, "EntityID")).Value);
            if (attemp.StudentID != thisStudentId)
            {
                return HttpUnauthorized();
            }
            var testSession = _unitOfWork.TestSessionRepository.GetByID(attemp.TestSessionID);
            var currentTime = DateTime.UtcNow;
            if ((testSession.StartTime == null || (testSession.StartTime != null && currentTime > testSession.StartTime.Value)) &&
                (testSession.EndTime == null || (testSession.EndTime != null && currentTime < testSession.EndTime.Value)))
            {
                return HttpBadRequest(new ErrorModel("Wrong Data", "Test session expired or has not been started yet."));
            }
            _unitOfWork.TaskSolutionRepository.Insert(taskSolution.ToEntity());
            _unitOfWork.Save();
            return Ok();
        }

        [HttpPut("{id:int}")]
        [Authorize("Teacher")]
        public IActionResult Put(int id, [FromBody]TaskSolutionModel taskSolution)
        {
            var taskSolutionToUpdate = _unitOfWork.TaskSolutionRepository.GetByID(id);
            if (taskSolutionToUpdate == null)
            {
                return HttpBadRequest(new ErrorModel("Wrong Data", $"Task solution with ID {id} does not exists in the DB."));
            }
            var validationResult = Validate(taskSolution);
            if (!string.IsNullOrEmpty(validationResult.Error))
            {
                return HttpBadRequest(validationResult);
            }
            if (taskSolution.StudentTestAttempID != taskSolutionToUpdate.StudentTestAttempID)
            {
                return HttpBadRequest(new ErrorModel("Wrong Data", "Can not update StudentTestAttemp field."));
            }
            if (taskSolution.TestingTaskID != taskSolutionToUpdate.TestingTaskID)
            {
                return HttpBadRequest(new ErrorModel("Wrong Data", "Can not update TestingTaskID field."));
            }
            taskSolutionToUpdate.Answer = taskSolution.Answer;
            taskSolutionToUpdate.CheckResult = taskSolution.CheckResult;
            taskSolutionToUpdate.IsSubmitted = taskSolution.IsSubmitted;
            _unitOfWork.TaskSolutionRepository.Update(taskSolutionToUpdate);
            _unitOfWork.Save();
            return Ok();
        }

        [HttpDelete("{id:int}")]
        [Authorize("Teacher")]
        public IActionResult Delete(int id)
        {
            _unitOfWork.TaskSolutionRepository.Delete(id);
            _unitOfWork.Save();
            return Ok();
        }

        private ErrorModel Validate(TaskSolutionModel taskSolution)
        {
            // Solution or Answer is null
            if (taskSolution?.Answer == null)
            {
                return new ErrorModel("Wrong data", "Model is null or Answer is null.");
            }
            // StudentAttemp or TestingTask not exists in the DB.
            if (_unitOfWork.StudentTestAttempRepository.GetByID(taskSolution.StudentTestAttempID) == null)
            {
                return
                    new ErrorModel("Wrong data",
                        $"Student attemp with ID {taskSolution.StudentTestAttempID} not exists in the DB");
            }
            if (_unitOfWork.TestingTaskRepository.GetByID(taskSolution.TestingTaskID) == null)
            {
                return
                    new ErrorModel("Wrong data",
                        $"Testing task with ID {taskSolution.TestingTaskID} not exists in the DB");
            }
            // No validation error
            return new ErrorModel();
        }
    }
}
