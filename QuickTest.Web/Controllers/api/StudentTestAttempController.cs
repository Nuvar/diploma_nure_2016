﻿using System;
using System.Linq;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using QuickTest.Web.DAL;
using QuickTest.Web.Models;
using QuickTest.Web.Models.Entities;
using QuickTest.Web.Models.EntityModels;

namespace QuickTest.Web.Controllers.api
{
    [Route("api/[controller]")]
    public class StudentTestAttempController : Controller
    {
        private readonly UnitOfWork _unitOfWork;

        public StudentTestAttempController(QuickTestDbContext dbContext)
        {
            _unitOfWork = new UnitOfWork(dbContext);
        }

        [HttpGet]
        [Authorize("Teacher")]
        public IActionResult Get()
        {
            var result = _unitOfWork.StudentTestAttempRepository.Get().ToList();
            if (!result.Any())
            {
                return HttpNotFound();
            }
            return Ok(result.ToList());
        }

        [HttpGet("{id:int}")]
        [Authorize("Teacher")]
        public IActionResult Get(int id)
        {
            var result = _unitOfWork.StudentTestAttempRepository.GetByID(id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return Ok(result);
        }

        [HttpGet("My")]
        [Authorize("Student")]
        public IActionResult MyAttemps()
        {
            var thisStudentId = Convert.ToInt32(User.Claims.Single(c => string.Equals(c.Type, "EntityID")).Value);
            var result = _unitOfWork.StudentTestAttempRepository.Get(attemp => attemp.Student.ID == thisStudentId).ToList();
            if (!result.Any())
            {
                return HttpNotFound();
            }
            return Ok(result);
        }

        [HttpPost]
        [Authorize("Teacher")]
        public IActionResult Post([FromBody]StudentTestAttempModel studentTestAttempModel)
        {
            var validationResult = Validate(studentTestAttempModel);
            if (!string.IsNullOrEmpty(validationResult.Error))
            {
                return HttpBadRequest(validationResult);
            }
            var testSession = _unitOfWork.TestSessionRepository.GetByID(studentTestAttempModel.TestSessionID);
            var currentTime = DateTime.UtcNow;
            if ((testSession.StartTime == null || (testSession.StartTime != null && currentTime > testSession.StartTime.Value)) &&
                (testSession.EndTime == null || (testSession.EndTime != null && currentTime < testSession.EndTime.Value)))
            {
                return HttpBadRequest(new ErrorModel("Wrong Data", "Test session expired or has not been started yet."));
            }
            var studentTestAttemp = studentTestAttempModel.ToEntity();
            var thisTeacherId = Convert.ToInt32(User.Claims.Single(c => string.Equals(c.Type, "EntityID")).Value);
            studentTestAttemp.TeacherWhoAllowedID = thisTeacherId;
            studentTestAttemp.TeacherWhoAllowed = _unitOfWork.TeacherRepository.GetByID(thisTeacherId);
            studentTestAttemp.IsUsed = false;
            _unitOfWork.StudentTestAttempRepository.Insert(studentTestAttempModel.ToEntity());
            _unitOfWork.Save();
            return Ok();
        }

        [HttpPost("start/{id:int}")]
        [Authorize("Student")]
        public IActionResult Start(int id)
        {
            var attemp = _unitOfWork.StudentTestAttempRepository.GetByID(id);
            if (attemp == null)
            {
                return HttpBadRequest(new ErrorModel("Wrong Data", $"Attemp with ID {id} does not exists in the DB."));
            }
            if (!string.Equals(HttpContext.User.Identity.Name, attemp.Student.Name, StringComparison.OrdinalIgnoreCase))
            {
                return HttpUnauthorized();
            }
            if (attemp.IsUsed)
            {
                return HttpBadRequest(new ErrorModel("Wrong Data", "Attemp has already been used."));
            }
            var testSession = _unitOfWork.TestSessionRepository.GetByID(attemp.TestSessionID);
            var currentTime = DateTime.UtcNow;
            if ((testSession.StartTime == null || (testSession.StartTime != null && currentTime > testSession.StartTime.Value)) &&
                (testSession.EndTime == null || (testSession.EndTime != null && currentTime < testSession.EndTime.Value)))
            {
                return HttpBadRequest(new ErrorModel("Wrong Data", "Test session expired or has not been started yet."));
            }
            attemp.StartTime = DateTime.UtcNow;
            attemp.IsUsed = true;
            return Ok();
        }

        [HttpPut("{id:int}")]
        [Authorize("Teacher")]
        public IActionResult Put(int id, [FromBody]StudentTestAttempModel attemp)
        {
            var attempToUpdate = _unitOfWork.StudentTestAttempRepository.GetByID(id);
            if (attempToUpdate == null)
            {
                return HttpBadRequest(new ErrorModel("Wrong Data", $"Attemp with ID {id} does not exists in the DB."));
            }
            if (!string.Equals(HttpContext.User.Identity.Name, attempToUpdate.TeacherWhoAllowed.UserName, StringComparison.OrdinalIgnoreCase))
            {
                return HttpUnauthorized();
            }
            if (attempToUpdate.IsUsed != attemp.IsUsed)
            {
                return HttpBadRequest(new ErrorModel("Wrong Data", "Can not update IsUsed field."));
            }
            attempToUpdate.StartTime = attemp.StartTime;
            attempToUpdate.StudentID = attemp.StudentID;
            attempToUpdate.TestSessionID = attemp.TestSessionID;
            _unitOfWork.StudentTestAttempRepository.Update(attempToUpdate);
            _unitOfWork.Save();
            return Ok();
        }

        [HttpDelete("{id:int}")]
        [Authorize("Teacher")]
        public IActionResult Delete(int id)
        {
            var attemp = _unitOfWork.StudentTestAttempRepository.GetByID(id);
            if (!string.Equals(HttpContext.User.Identity.Name, attemp.TeacherWhoAllowed.UserName, StringComparison.OrdinalIgnoreCase))
            {
                return HttpUnauthorized();
            }
            _unitOfWork.StudentTestAttempRepository.Delete(id);
            _unitOfWork.Save();
            return Ok();
        }

        private ErrorModel Validate(StudentTestAttempModel studentTestAttemp)
        {
            // Student not exists in Database.
            if (_unitOfWork.StudentRepository.GetByID(studentTestAttemp.StudentID) == null ||
                _unitOfWork.TestSessionRepository.GetByID(studentTestAttemp.TestSessionID) == null)
            {
                return
                    new ErrorModel("Wrong Data",
                        $"User or test session not exists in the DataBase.");
            }
            // No validation error
            return new ErrorModel();
        }
    }
}
