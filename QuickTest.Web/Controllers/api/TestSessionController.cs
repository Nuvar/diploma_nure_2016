﻿using System;
using System.Linq;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using QuickTest.Web.DAL;
using QuickTest.Web.Models;
using QuickTest.Web.Models.Entities;
using QuickTest.Web.Models.EntityModels;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Threading;
using QuickTest.Web.Services;

namespace QuickTest.Web.Controllers.api
{
    [Route("api/[controller]")]
    public class TestSessionController : Controller
    {
        private readonly UnitOfWork _unitOfWork;

        public TestSessionController(QuickTestDbContext dbContext)
        {
            _unitOfWork = new UnitOfWork(dbContext);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = _unitOfWork.TestSessionRepository.Get().ToList();
            if (!result.Any())
            {
                return HttpNotFound();
            }
            return Ok(result.ToList());
        }

        [HttpGet("{id:int}")]
        [Authorize("User")]
        public IActionResult Get(int id)
        {
            var result = _unitOfWork.TestSessionRepository.GetByID(id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return Ok(result);
        }

        [HttpGet("Start/{id:int}")]
        [Authorize("Student")]
        public IActionResult Start(int id)
        {
            var result = Get(id);
            if(result is HttpOkObjectResult)
            {
                int studentId = Convert.ToInt32(User.Claims.Single(c => string.Equals(c.Type, "EntityID")).Value);
                Timer timer = new Timer(state =>
                {
                    WebSocketHub.HandleCommand(null, new SocketCommandEventArgs
                    {
                        CommandName = "CloseSession",
                        Args = new[] { id.ToString(), studentId.ToString() }
                    });
                }, null, ((TestSession)(result as HttpOkObjectResult).Value).TestTime, Timeout.InfiniteTimeSpan);
            }

            return result;
        }

        [HttpGet("StudentsList/{sessionId:int}")]
        public IActionResult GetStudents(int sessionId)
        {
            var result = _unitOfWork.TestSessionRepository.GetByID(sessionId);
            if (result == null)
            {
                return HttpNotFound();
            }
            return Ok(result.StudentAttemps.Select(s => new StudentOnTestModel { Name = _unitOfWork.StudentRepository.GetByID(s.StudentID).Name, Id = s.ID }).ToList());
        }

        [HttpPost]
        [Authorize("Teacher")]
        public IActionResult Post([FromBody]TestSessionModel testSession)
        {
            var validationResult = Validate(testSession);
            if (!string.IsNullOrEmpty(validationResult.Error))
            {
                return HttpBadRequest(validationResult);
            }
            var testSessionToCreate = testSession.ToEntity();
            int creatorId = Convert.ToInt32(User.Claims.Single(c => string.Equals(c.Type, "EntityID")).Value);
            testSessionToCreate.CreatorID = creatorId;
            var students = testSession.Students.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            testSessionToCreate.StudentAttemps = new List<StudentTestAttemp>();
            foreach(string name in students)
            {
                string normilizedName = name.Trim();
                normilizedName = Regex.Replace(normilizedName, @"\s{2,}", " ");
                var student = new Student() { Name = normilizedName, CreatedDate = DateTime.Now, ModifiedDate = DateTime.Now };
                _unitOfWork.StudentRepository.Insert(student);
                var studentAttemp = new StudentTestAttemp()
                {
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    IsUsed = false,
                    Student = student,
                    TeacherWhoAllowedID = creatorId,
                };
                testSessionToCreate.StudentAttemps.Add(studentAttemp);
            }
            _unitOfWork.TestSessionRepository.Insert(testSessionToCreate);
            _unitOfWork.Save();
            return Ok(testSessionToCreate);
        }

        [HttpPut("{id:int}")]
        [Authorize("Teacher")]
        public IActionResult Put(int id, [FromBody]TestSessionModel testSession)
        {
            var testSessionToUpdate = _unitOfWork.TestSessionRepository.GetByID(id);
            if (testSessionToUpdate == null)
            {
                return HttpBadRequest(new ErrorModel("Wrong Data", $"Test Session with ID {id} does not exists in the DB."));
            }
            var validationResult = Validate(testSession);
            if (!string.IsNullOrEmpty(validationResult.Error))
            {
                return HttpBadRequest(validationResult);
            }
            testSessionToUpdate.StartTime = testSession.StartTime;
            testSessionToUpdate.EndTime = testSession.EndTime;
            testSessionToUpdate.TestTime = TimeSpan.FromMinutes(testSession.TestTime);
            testSessionToUpdate.StartTime = testSession.StartTime;
            testSessionToUpdate.StartTime = testSession.StartTime;
            _unitOfWork.TestSessionRepository.Update(testSessionToUpdate);
            _unitOfWork.Save();
            return Ok();
        }

        [HttpDelete("{id:int}")]
        [Authorize("Teacher")]
        public IActionResult Delete(int id)
        {
            _unitOfWork.TestSessionRepository.Delete(id);
            _unitOfWork.Save();
            return Ok();
        }

        private ErrorModel Validate(TestSessionModel testSession)
        {
            // Test session or TestTime is null
            if (testSession?.TestTime == null)
            {
                return new ErrorModel("Wrong data", "Model is null or Title is null.");
            }
            // Test session not Exists
            if (_unitOfWork.TestRepository.GetByID(testSession.TestID) == null)
            {
                return new ErrorModel("Wrong data", $"Test with ID {testSession.TestID} does not exists in the DB.");
            }
            // No validation error
            return new ErrorModel();
        }
    }
}
