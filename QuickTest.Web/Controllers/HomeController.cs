﻿using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;

namespace QuickTest.Web.Controllers
{
    public class HomeController : Controller
    {
        //[Authorize("Teacher")]
        public IActionResult Home()
        {
            return View();
        }

        
        //public IActionResult SignIn()
        //{
        //    return View();
        //}

        //[Authorize("Student")]
        //public IActionResult Trial()
        //{
        //    return View();
        //}
    }
}