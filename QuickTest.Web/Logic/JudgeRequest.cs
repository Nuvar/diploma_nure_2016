﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QuickTest.Web.Models;
using QuickTest.Web.Models.EntityModels;

namespace QuickTest.Web.Logic
{
    public static class JudgeRequest
    {
        public static RequestResult<IEnumerable<TaskInfoModel>> GetTasks(string token)
        {
            var judgeResponse = SendGetRequest("Tss/api/Task", token);
            if (!judgeResponse.Success)
            {
                return new RequestResult<IEnumerable<TaskInfoModel>>(judgeResponse.StatusCode, judgeResponse.Error);
            }
            var result = JsonConvert.DeserializeObject<IEnumerable<TaskInfoModel>>(judgeResponse.Result);
            return new RequestResult<IEnumerable<TaskInfoModel>>(judgeResponse.StatusCode, result);
        }

        public static RequestResult<TaskFullModel> GetFullTaskByID(int id, string token)
        {
            var judgeResponse = SendGetRequest($"Tss/api/Task/{id}", token);
            if (!judgeResponse.Success)
            {
                return new RequestResult<TaskFullModel>(judgeResponse.StatusCode, judgeResponse.Error);
            }
            var result = JsonConvert.DeserializeObject<TaskFullModel>(judgeResponse.Result);
            return new RequestResult<TaskFullModel>(judgeResponse.StatusCode, result);
        }

        public static RequestResult<TaskInfoModel> GetTaskInfoByID(int id)
        {
            var judgeResponse = SendGetRequest($"Tss/api/Check/{id}");
            if (!judgeResponse.Success)
            {
                return new RequestResult<TaskInfoModel>(judgeResponse.StatusCode, judgeResponse.Error);
            }
            var result = JsonConvert.DeserializeObject<TaskInfoModel>(judgeResponse.Result);
            return new RequestResult<TaskInfoModel>(judgeResponse.StatusCode, result);
        }

        public static RequestResult<string> GetAuthorTaskSolution(int id, string token)
        {
            var judgeResponse = SendPostRequest($"Tss/api/Task/{id}", "", token);
            if (!judgeResponse.Success)
            {
                return new RequestResult<string>(judgeResponse.StatusCode, judgeResponse.Error);
            }
            var result = JsonConvert.DeserializeObject<string>(judgeResponse.Result);
            return new RequestResult<string>(judgeResponse.StatusCode, result);
        }

        public static RequestResult<JudgeTokenModel> GetJudgeAuthToken(string login, string password)
        {
            var judgeResponse = SendPostRequest("Tss/Token", $"grant_type=password&username={login}&password={password}");
            if (!judgeResponse.Success)
            {
                return new RequestResult<JudgeTokenModel>(judgeResponse.StatusCode, judgeResponse.Error);
            }
            dynamic judgeToken =
                    JObject.Parse(
                        judgeResponse.Result
                            .Replace(".expires", "expires")
                            .Replace(".issued", "issued"));
            var result = new JudgeTokenModel
            {
                AccessToken = judgeToken.access_token.ToString(),
                Expires = DateTime.Parse(judgeToken.expires.ToString()),
                ExpiresIn = Convert.ToInt32(judgeToken.expires_in.ToString()),
                Issued = DateTime.Parse(judgeToken.issued.ToString()),
                TokenType = judgeToken.token_type.ToString(),
                UserName = judgeToken.userName.ToString()
            };
            return new RequestResult<JudgeTokenModel>(judgeResponse.StatusCode, result);
        }

        public static RequestResult<TeacherModel> GetJudgeUserInfo(string token)
        {
            var judgeResponse = SendGetRequest("Tss/api/Account/UserInfo", token);
            if (!judgeResponse.Success)
            {
                return new RequestResult<TeacherModel>(judgeResponse.StatusCode, judgeResponse.Error);
            }
            dynamic userInfo = JObject.Parse(judgeResponse.Result);
            var result = new TeacherModel
            {
                UserName = userInfo.UserName.ToString(),
                HasRegistered = userInfo.HasRegistered.ToString() == "true",
                LoginProvider = userInfo.LoginProvider?.ToString()
            };
            return new RequestResult<TeacherModel>(judgeResponse.StatusCode, result);
        }

        public static RequestResult<string> CheckTask(int id, string solution)
        {
            var judgeResponse = SendPostRequest($"Tss/api/Check/{id}", solution);
            if (!judgeResponse.Success)
            {
                return new RequestResult<string>(judgeResponse.StatusCode, judgeResponse.Error);
            }
            var result = JsonConvert.DeserializeObject<string>(judgeResponse.Result);
            return new RequestResult<string>(judgeResponse.StatusCode, result);
        }

        public static RequestResult<string> CheckCode(string language, string code)
        {
            var judgeResponse = SendPutRequest($"Tss/api/Check/{language}", code);
            if (!judgeResponse.Success)
            {
                return new RequestResult<string>(judgeResponse.StatusCode, judgeResponse.Error);
            }
            var result = JsonConvert.DeserializeObject<string>(judgeResponse.Result);
            return new RequestResult<string>(judgeResponse.StatusCode, result);
        }


        static RequestResult<string> SendGetRequest(string path, string token = null)
        {
            RequestResult<string> result;
            using (var client = new HttpClient())
            {
                if (!string.IsNullOrEmpty(token))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                }
                var responseResult = client.GetAsync($"{JudgeHostAddress}/{path}").Result;
                var resultContent = responseResult.Content.ReadAsStringAsync().Result;
                var responseStatusCode = (int) responseResult.StatusCode;
                if (responseStatusCode >= 200 && responseStatusCode <= 299)
                {
                    result = new RequestResult<string>(responseStatusCode, resultContent);
                }
                else
                {
                    result = new RequestResult<string>(responseStatusCode, new ErrorModel("Judge Error", resultContent));
                }
            }
            return result;
        }

        static RequestResult<string> SendPostRequest(string path, string contentBody, string token = null)
        {
            RequestResult<string> result;
            using (var client = new HttpClient())
            {
                var content = new StringContent(contentBody);
                if (!string.IsNullOrEmpty(token))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                }
                var responseResult = client.PostAsync($"{JudgeHostAddress}/{path}", content).Result;
                var resultContent = responseResult.Content.ReadAsStringAsync().Result;
                var responseStatusCode = (int)responseResult.StatusCode;
                if (responseStatusCode >= 200 && responseStatusCode <= 299)
                {
                    result = new RequestResult<string>(responseStatusCode, resultContent);
                }
                else
                {
                    result = new RequestResult<string>(responseStatusCode, new ErrorModel("Judge Error", resultContent));
                }
            }
            return result;
        }

        static RequestResult<string> SendPutRequest(string path, string contentBody, string token = null)
        {
            RequestResult<string> result;
            using (var client = new HttpClient())
            {
                var content = new StringContent(contentBody);
                if (!string.IsNullOrEmpty(token))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                }
                var responseResult = client.PutAsync($"{JudgeHostAddress}/{path}", content).Result;
                var resultContent = responseResult.Content.ReadAsStringAsync().Result;
                var responseStatusCode = (int)responseResult.StatusCode;
                if (responseStatusCode >= 200 && responseStatusCode <= 299)
                {
                    result = new RequestResult<string>(responseStatusCode, resultContent);
                }
                else
                {
                    result = new RequestResult<string>(responseStatusCode, new ErrorModel("Judge Error", resultContent));
                }
            }
            return result;
        }

        static string JudgeHostAddress
        {
            get
            {
                var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
                var configuration = builder.Build();
                return configuration["JudgeServer"];
            }
        }
    }
}
