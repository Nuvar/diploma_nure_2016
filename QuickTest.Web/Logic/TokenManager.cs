﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using QuickTest.Web.Models;
using System.IdentityModel.Tokens;
using System.Collections.Generic;

namespace QuickTest.Web.Logic
{
    public static class TokenManager
    {
        public static TokenModel GetToken(string userName, int userId, UserRole role, TokenAuthOptions tokenOptions)
        {
            var handler = new JwtSecurityTokenHandler();

            var expires = DateTime.UtcNow;

            if (role == UserRole.Teacher)
            {
                expires = expires.AddMonths(2);
            }
            if (role == UserRole.Student)
            {
                expires = expires.AddMonths(2);
            }

            // Here, you should create or look up an identity for the user which is being authenticated.
            // For now, just creating a simple generic identity.
            var identity = new ClaimsIdentity(
                new GenericIdentity(userName, "TokenAuth"),
                new[]
                {
                    new Claim("EntityID", userId.ToString(), ClaimValueTypes.Integer),
                    new Claim(ClaimTypes.Role, role.ToString("G"))
                });

            var securityToken = handler.CreateToken(
                issuer: tokenOptions.Issuer,
                audience: tokenOptions.Audience,
                signingCredentials: tokenOptions.SigningCredentials,
                subject: identity,
                expires: expires
                );
            return new TokenModel(userName, userId, role, handler.WriteToken(securityToken), expires);
        }

        public static IEnumerable<Claim> ReadUserFromToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();

            JwtSecurityToken parsedToken = handler.ReadJwtToken(token);
            return parsedToken.Claims;
        }
    }
}
